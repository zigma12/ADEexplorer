import typing

from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt
from PyQt5.QtGui import QIcon, QColor, QFont, QPixmap
from PyQt5.QtWidgets import *
from ADEcore import ADEevent, ADEcalendar

import pathsManagement as pM


class NoCollisionError(Exception):
    pass


class Collision:
    def __init__(self, event1: ADEevent, event2: ADEevent, hidden: bool = False):
        if not self.collide(event1, event2):
            raise NoCollisionError(f"Events {event1} and {event2} do not collide.")
        if event2.uid.startswith('ADE') and not event1.uid.startswith('ADE'):
            event1, event2 = event2, event1
        self.event2 = event2
        self.event1 = event1
        self.hidden = False

    def begins_first(self):
        if self.event2.begin < self.event1.begin:
            return self.event2
        return self.event1

    def begins_last(self):
        if self.event1.begin > self.event2.begin:
            return self.event1
        return self.event2

    def ends_last(self):
        if self.event1.end > self.event2.end:
            return self.event1
        return self.event2

    def ends_first(self):
        if self.event2.end < self.event1.end:
            return self.event2
        return self.event1

    def begin(self):
        return max(self.event1.begin, self.event2.begin)

    def end(self):
        return min(self.event1.end, self.event2.end)

    def duration(self):
        return self.end() - self.begin()

    def signature(self):
        return self.event1.uid, self.event2.uid

    def mathes_signature(self, uid1: str, uid2: str):
        return (self.event1.uid == uid1 and self.event2.uid == uid2 or
                self.event1.uid == uid2 and self.event2.uid == uid1)

    @classmethod
    def intercalendar_collision_list(cls, *args, after=None) -> list:
        if not all([isinstance(arg, ADEcalendar) for arg in args]):
            raise NotImplementedError("At least one argument is not an ADEcalendar instance.")
        ret: list[cls] = list()
        if len(args) < 2:
            return ret
        for i1, cal1 in enumerate(args[:-1]):
            for cal2 in args[i1 + 1:]:
                assert isinstance(cal1, ADEcalendar)
                assert isinstance(cal2, ADEcalendar)
                for event1 in cal1.events:
                    if after and event1.end < after:
                        continue
                    for event2 in cal2.events:
                        if after and event2.end < after:
                            continue
                        try:
                            ret.append(cls(event1, event2))
                        except NoCollisionError:
                            pass
        return sorted(ret)

    @classmethod
    def colliding_events(cls, *args) -> list:
        if not all([isinstance(arg, ADEevent) for arg in args]):
            raise NotImplementedError("At least one argument is not an ADEevent instance.")
        ret: list[cls] = list()
        if len(args) < 2:
            return ret
        for i1, event1 in enumerate(args[:-1]):
            for event2 in args[i1 + 1:]:
                try:
                    ret.append(cls(event1, event2))
                except NoCollisionError:
                    pass
        return sorted(ret)

    def __eq__(self, other):
        if type(other) != type(self):
            return False
        return self.__hash__() == other.__hash__()

    def __lt__(self, other):
        if type(other) != type(self):
            return False
        return self.begin() < other.begin()

    def __repr__(self):
        return f"Collision: event1={self.event1.name} event2={self.event2.name}"

    def __hash__(self):
        return hash(hash(self.event1.uid) + hash(self.event2.uid))

    @staticmethod
    def collide(event1: ADEevent, event2: ADEevent) -> bool:
        return max(event1.begin, event2.begin) < min(event1.end, event2.end)


class CollisionTableModel(QAbstractTableModel):
    def __init__(self, collisions: list[Collision]):
        super(CollisionTableModel, self).__init__()
        self._collisions = collisions

    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...) -> typing.Any:
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                if section == 0:
                    return "Date"
                if section == 1:
                    return "Durée"
                if section == 2:
                    return "Événement 1"
                if section == 3:
                    return "Événement 2"
        if orientation == Qt.Vertical:
            if role == Qt.DecorationRole:
                if self._collisions[section].hidden :
                    return QIcon(pM.icon("alarm_off.svg"))
                else:
                    return QIcon(pM.icon("alarm_on.svg"))

    def data(self, index: QModelIndex, role: int = ...) -> typing.Any:
        col = index.column()
        row = index.row()
        if role == Qt.DisplayRole:
            if col == 0:
                return self._collisions[row].begin().format("DD/MM/YYYY")
            if col == 1:
                return str(self._collisions[row].duration())
            if col == 2:
                return self._collisions[row].event1.name
            if col == 3:
                return self._collisions[row].event2.name
        if role == Qt.ForegroundRole:
            if self._collisions[row].hidden:
                return QColor(Qt.darkGray)
        if role == Qt.FontRole:
            font = QFont()
            if self._collisions[row].hidden:
                font.setItalic(True)
            else:
                font.setBold(True)
            return font
        if role == Qt.ToolTipRole:
            if col == 1:
                return ("Début : " + str(self._collisions[row].begin().to(ADEcalendar.TimeZone).format("DD/MM/YYYY à HH:mm")) + '\n' +
                        "Fin : " + str(self._collisions[row].end().to(ADEcalendar.TimeZone).format("DD/MM/YYYY à HH:mm")))
            if col == 2:
                return str(self._collisions[row].event1.tool_tip())
            if col == 3:
                return str(self._collisions[row].event2.tool_tip())

    def rowCount(self, parent: QModelIndex = ...) -> int:
        return len(self._collisions)

    def columnCount(self, parent: QModelIndex = ...) -> int:
        return 4

    def collisionStateToggle(self, section: int):
        self._collisions[section].hidden = not self._collisions[section].hidden
        self.dataChanged.emit(self.index(section, 0), self.index(section, self.rowCount()))


class CollisionsDialog(QDialog):
    def __init__(self, collisions: list[Collision], cal_name: str = None, showHidden: bool = True, *args, **kwargs):
        super(CollisionsDialog, self).__init__(*args, **kwargs)

        self.setWindowTitle("Recherche de collisions...")
        self.resize(950, 400)
        self.verticalLayout = QVBoxLayout(self)
        self.text = QLabel(
            f"<p>Les agendas de la ressource <b>{cal_name}</b> présentent {len(collisions)} événement(s) "
            f"en collision (événements programmés sur la même plage horaire).</p>"
            f"<p>Cliquer sur l'icône d'une ligne pour activer <img width='24' src='{pM.icon('alarm_on.svg')}'/> ou "
            f"désactiver <img width='24' src='{pM.icon('alarm_off.svg')}'/> l'alarme de la collision associée.</p>")
        self.text.setWordWrap(True)
        self.collisionTable = QTableView(self)
        self.collisionModel = CollisionTableModel(collisions)
        self.collisionTable.setModel(self.collisionModel)
        self.collisionTable.setSelectionMode(QAbstractItemView.NoSelection)

        Hheader = self.collisionTable.horizontalHeader()
        Hheader.setSectionResizeMode(QHeaderView.ResizeToContents)
        Hheader.setStretchLastSection(True)
        Vheader = self.collisionTable.verticalHeader()
        # Vheader.setDefaultSectionSize(60)
        Vheader.sectionClicked.connect(self.collisionModel.collisionStateToggle)

        self.nameFont = QApplication.font()
        self.nameFont.setPointSize(9)
        self.collisionTable.setFont(self.nameFont)

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel)
        self.verticalLayout.addWidget(self.text)
        self.verticalLayout.addWidget(self.collisionTable)
        self.verticalLayout.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

