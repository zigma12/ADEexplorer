import sys

from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import QObject, QTimer
from PyQt5.QtGui import QColor, QPalette
from PyQt5.QtWidgets import QGraphicsOpacityEffect


def dest(o: QObject):
    pass
    # print("DESTROYED:", o.objectName())


class EventWidget(QtWidgets.QPushButton):
    topArrow = 1
    bottomArrow = 2

    def __init__(self, parent=None, rotation="none", text='',
                 arrow_position=None, isFreeTime=False):
        super(EventWidget, self).__init__(text, parent)
        self.isFreeTime = isFreeTime
        self.setParent(None)
        self.destroyed.connect(dest)
        self.rotation = rotation
        self.fm = self.fontMetrics()
        self.fullText = text
        self.arrow = arrow_position
        self.stacked_under: EventWidget = None
        self.oneShotTimer = QTimer(self)
        self.oneShotTimer.setSingleShot(True)

    def setColor(self, color: QColor):
        pal = self.palette()
        pal.setColor(QPalette.Button, color)
        self.setAutoFillBackground(True)
        self.setPalette(pal)
        self.update()

    def setOpacity(self, opacity: float):
        op = QGraphicsOpacityEffect(self)
        op.setOpacity(opacity)  # 0 to 1 will cause the fade effect to kick in
        self.setGraphicsEffect(op)

    def setFont(self, a0: QtGui.QFont) -> None:
        super(EventWidget, self).setFont(a0)
        self.fm = self.fontMetrics()

    def setText(self, text: str) -> None:
        self.fullText = text
        super(EventWidget, self).setText(text)

    def textWidth(self):
        return max([self.fm.horizontalAdvance(line, -1) for line in self.fullText.split('\n')])

    def textHeight(self):
        return self.fm.tightBoundingRect(self.text()).height()

    def elideText(self):
        hSize, vSize = (self.width(), self.height()) if self.rotation == 'none' else (self.height(), self.width())
        out_text = ''
        if sys.platform == 'win32':
            hfactor, vfactor = 0.95, 0.90
        else:
            hfactor, vfactor = 1.0, 0.98
        for n, line in enumerate(self.fullText.split(sep='\n')):
            line = self.fm.elidedText(line, QtCore.Qt.TextElideMode.ElideRight, int(hfactor * hSize))
            out_text += f'\n{line}'
            if (n + 2) * self.fm.lineSpacing() > vfactor * vSize:
                break
        out_text = out_text.strip('\n')
        super(EventWidget, self).setText(out_text)

    def updateOrientation(self):
        if self.width() <= self.height() and self.textWidth() >= self.width():
            self.rotation = 'east'
        else:
            self.rotation = 'none'

    def paintEvent(self, event):
        self.updateOrientation()
        self.elideText()
        painter = QtWidgets.QStylePainter(self)
        if self.rotation == "none":
            painter.rotate(0)
        elif self.rotation == "east":
            painter.rotate(-90)
            painter.translate(-1 * self.height(), 0)
        elif self.rotation == "west":
            painter.rotate(90)
            painter.translate(0, -1 * self.width())

        painter.drawControl(QtWidgets.QStyle.ControlElement.CE_PushButton, self.getStyleOptions())

    def enterEvent(self, e: QtCore.QEvent) -> None:
        self.oneShotTimer.timeout.connect(self.raise_)
        self.oneShotTimer.start(300)
        super(EventWidget, self).enterEvent(e)

    def leaveEvent(self, e: QtCore.QEvent) -> None:
        try:
            self.oneShotTimer.timeout.disconnect()
        except TypeError:
            pass
        if self.stacked_under:
            self.stackUnder(self.stacked_under)
        super(EventWidget, self).leaveEvent(e)

    def minimumSizeHint(self):
        size = super(EventWidget, self).minimumSizeHint()
        if self.rotation != 'none':
            size.transpose()
        return size

    def sizeHint(self):
        size = super(EventWidget, self).sizeHint()
        if self.rotation != 'none':
            size.transpose()
        return size

    def getStyleOptions(self):
        options = QtWidgets.QStyleOptionButton()
        options.initFrom(self)
        size = options.rect.size()
        if self.rotation != 'none':
            size.transpose()
        options.rect.setSize(size)
        options.features = QtWidgets.QStyleOptionButton.ButtonFeature.None_
        if self.isFlat():
            options.features |= QtWidgets.QStyleOptionButton.ButtonFeature.Flat
        if self.menu():
            options.features |= QtWidgets.QStyleOptionButton.ButtonFeature.HasMenu
        if self.autoDefault() or self.isDefault():
            options.features |= QtWidgets.QStyleOptionButton.ButtonFeature.AutoDefaultButton
        if self.isDefault():
            options.features |= QtWidgets.QStyleOptionButton.ButtonFeature.DefaultButton
        if self.isDown() or (self.menu() and self.menu().isVisible()):
            options.state |= QtWidgets.QStyle.StateFlag.State_Sunken
        if self.isChecked():
            options.state |= QtWidgets.QStyle.StateFlag.State_On
        if not self.isFlat() and not self.isDown():
            options.state |= QtWidgets.QStyle.StateFlag.State_Raised

        options.text = self.text()
        options.icon = self.icon()
        options.iconSize = self.iconSize()
        return options


class Main(QtWidgets.QFrame):
    def __init__(self):
        QtWidgets.QFrame.__init__(self)
        self.application = QtCore.QCoreApplication.instance()
        self.layout = QtWidgets.QHBoxLayout()
        self.button1 = EventWidget(self, rotation="east", text="east")
        self.button2 = EventWidget(self, rotation="west", text="west")
        self.button3 = EventWidget(self, rotation="none", text="none")
        self.layout.addWidget(self.button1)
        self.layout.addWidget(self.button2)
        self.layout.addWidget(self.button3)
        self.setLayout(self.layout)


if __name__ == '__main__':
    application = QtWidgets.QApplication(sys.argv)
    application.main = Main()
    application.main.show()
    sys.exit(application.exec())
