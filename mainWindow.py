import errno
import os
import posixpath
import subprocess
import sys
import timeit
import urllib.parse
from copy import deepcopy
from datetime import timedelta

import arrow.arrow
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from anytree import PreOrderIter
from lxml import etree

import joursFeriesFrancais
from ADEcore import *
from collisions import CollisionsDialog, Collision
from dateTimeWidgets import DateRange
from displayCalendar import AgendaStyle, OneAgendaGraphicView, OneAgendaStatistics, OneAgendaTableView, \
    formatDuration_hhmm, takeScreenShot
from newYearWizard import NewYearWizard
from resourceTreeStructure import ResourceTreeStructure, ResourceTreeModel, ResourceNode, ResourceTreeView, \
    ExtraAgendasData
from settings import SettingDialog
from ui.mainWindowGUI import Ui_MainWindow

__ade_explorer_version__ = '1.4.3'
__gitlab__ = "https://gitlab.com/vinraspa/ADEexplorer/"


def dest(o: QObject):
    print("DESTROYED:", o, o.objectName())


class MissingCalendarError(Exception):
    pass


class VersionNotes(QDialog):
    def __init__(self):
        super(VersionNotes, self).__init__()
        self.resize(600, 500)
        self.verticalLayout = QVBoxLayout(self)
        self.text = QLabel()
        self.text.setWordWrap(True)
        self.informativeText = QLabel()
        self.informativeText.setWordWrap(True)
        self.versionNote = QTextEdit(self)
        self.versionNote.setTextInteractionFlags(Qt.TextInteractionFlag.TextSelectableByKeyboard |
                                                 Qt.TextInteractionFlag.TextSelectableByMouse)
        self.checkBox = QCheckBox()
        self.verticalLayout.addWidget(self.versionNote)
        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setOrientation(Qt.Orientation.Horizontal)
        # self.buttonBox.setStandardButtons(QDialogButtonBox.StandardButton.Cancel|QDialogButtonBox.StandardButton.Ok)
        self.verticalLayout.addWidget(self.text)
        self.verticalLayout.addWidget(self.informativeText)
        self.verticalLayout.addWidget(self.versionNote)
        self.verticalLayout.addWidget(self.checkBox)
        self.verticalLayout.addWidget(self.buttonBox)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def setText(self, text: str):
        self.text.setText(text)

    def setInformativeText(self, text: str):
        self.informativeText.setText(text)

    def setVersionNote(self, markdown: str):
        self.versionNote.setMarkdown(markdown)


class MainWindow(QMainWindow, Ui_MainWindow):
    _selectionRessourcesChanged_counter: int = 0

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self)

        # Initialisation du splashscreen
        nsmap = {
            # 'sodipodi': 'http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd',
            # 'cc': 'http://web.resource.org/cc/',
            'svg': 'http://www.w3.org/2000/svg',
            # 'dc': 'http://purl.org/dc/elements/1.1/',
            # 'xlink': 'http://www.w3.org/1999/xlink',
            # 'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
            # 'inkscape': 'http://www.inkscape.org/namespaces/inkscape'
        }
        with open(pM.rsc("splash.svg"), 'r') as svg:
            data = etree.XML(svg.read().encode('utf8'))
            for elt in data.xpath('//svg:text[@id="__ade_explorer_version__"]', namespaces=nsmap):
                elt.xpath('./svg:tspan', namespaces=nsmap)[0].text = 'v' + __ade_explorer_version__
        self.pixmap = QPixmap()
        self.pixmap.loadFromData(etree.tostring(data))
        self.splash = QSplashScreen(self.pixmap)
        self.splashMessageConfig = {'alignment': Qt.AlignmentFlag.AlignLeft | Qt.AlignmentFlag.AlignBottom,
                                    'color': QColor("white")}
        self.splash.show()

        # Initialisation de la GUI
        self.splash.showMessage("Initialisation de l'application...", **self.splashMessageConfig)
        self.setWindowTitle(f"ADEexplorer {__ade_explorer_version__}")

        # Association des icônes
        self.setWindowIcon(QIcon(pM.icon("ADEicon.svg")))
        self.action_agendaSnapshot.setIcon(QIcon(pM.icon("duplicate_agenda.svg")))
        self.action_screenshot.setIcon(QIcon(pM.icon("screenshot.svg")))
        self.action_affichageGraphique.setIcon(QIcon(pM.icon("affichage_graphique.svg")))
        self.action_affichageListe.setIcon(QIcon(pM.icon("affichage_liste.svg")))
        self.action_modeSimple.setIcon(QIcon(pM.icon("mode_simple.svg")))
        self.action_affichageStat.setIcon(QIcon(pM.icon("affichage_stat.svg")))
        self.action_modeFreetime.setIcon(QIcon(pM.icon("mode_free.svg")))
        self.action_modeTogether.setIcon(QIcon(pM.icon("mode_together.svg")))
        self.action_A_propos.setIcon(QIcon(pM.icon("ADEicon.svg")))
        self.action_contact.setIcon(QIcon(pM.icon("email.svg")))
        self.action_help.setIcon(QApplication.style().standardIcon(QStyle.StandardPixmap.SP_DialogHelpButton))
        self.actionPreferences.setIcon(QIcon(pM.icon("settings.svg")))
        self.actionReload_resources.setIcon(QIcon(pM.icon("reload_resources.svg")))
        self.actionEditerListeRessources.setIcon(QIcon(pM.icon("spreadsheet.svg")))
        self.action_clearCache.setIcon(QApplication.style().standardIcon(QStyle.StandardPixmap.SP_TrashIcon))
        self.action_update_check.setIcon((QIcon(pM.icon("update.svg"))))
        self.action_new_year_wizard.setIcon((QIcon(pM.icon("wizard_new_year.svg"))))
        self.pushButton_FiltreClear.setIcon(QIcon(pM.icon("edit-clear.svg")))

        # Création d'une barre de statut
        self.statusBar = QStatusBar(self)
        self.statusBar.setObjectName("statusBar")
        self.setStatusBar(self.statusBar)
        self.statusBarMainLabel = QLabel()
        self.statusBarMainLabel.setFrameShape(QFrame.Shape.Panel)
        self.statusBarMainLabel.setFrameShadow(QFrame.Shadow.Sunken)
        self.statusBarSecondLabel = QLabel()
        self.statusBarSecondLabel.setFrameShape(QFrame.Shape.Panel)
        self.statusBarSecondLabel.setFrameShadow(QFrame.Shadow.Sunken)
        self.statusBar.addWidget(self.statusBarMainLabel)
        self.statusBar.addWidget(self.statusBarSecondLabel)

        # Création d'une QProgressBar() associées à la barre de statut
        self.statusProgressBar = QProgressBar()
        self.statusProgressBar.setMaximumWidth(300)
        self.statusProgressBar.setFixedHeight(20)
        self.statusProgressBar.setMaximum(100)
        self.statusProgressBar.setValue(3)
        if sys.platform == 'win32':
            self.statusProgressBar.setStyleSheet("QProgressBar {"
                                                 "border: 2px solid grey;"
                                                 "border-radius: 5px;"
                                                 "text-align: center"
                                                 "}"
                                                 "QProgressBar::chunk {"
                                                 "background-color: #05B8CC;"
                                                 "width: 20px;"
                                                 "}")
        self.statusBar.addPermanentWidget(self.statusProgressBar)

        # Allocation des coins aux DockWidgetAreas
        self.setCorner(Qt.Corner.TopRightCorner, Qt.DockWidgetArea.RightDockWidgetArea)
        self.setCorner(Qt.Corner.TopLeftCorner, Qt.DockWidgetArea.LeftDockWidgetArea)

        # Déclaration des variables d'instance
        self.resource_file: str = ""
        self.resourceTreeStructure: ResourceTreeStructure | None = None
        self.resourceTreeViews: list[ResourceTreeView] = list()
        self.resourceTreeModels: list[ResourceTreeModel] = list()
        self.firstLaunch: bool = False
        self.eventMovePending: bool = False
        self.agendaSnapshots: list[OneAgendaGraphicView | OneAgendaTableView] = list()

        # Paramètres d'affichage des agendas
        self.oneAgenda_style = AgendaStyle.GRAPHIC
        self.oneAgenda_cal_to_display: list[ADEcalendar] = list()
        self.oneAgenda_begin_date: None | arrow.Arrow = None
        self.oneAgenda_end_date: None | arrow.Arrow = None
        self.oneAgenda_begin_day_time: None | arrow.Arrow = None
        self.oneAgenda_end_day_time: None | arrow.Arrow = None
        self.oneAgenda_resolution_in_minutes: int = 15
        self.oneAgenda_show_empty_weeks: bool = True
        self.oneAgenda_displayed_week_days: list[int] = list(range(5))
        self.oneAgenda_displayed_columns: list[str] = list()
        self.oneAgenda_holidays: list = list()
        self.oneAgenda_event_filter = EventFilter()

        # Définition du dossier où sont stockés les agendas téléchargés
        self.cache_directory = os.path.join(pM.exe_dir, ".cache", "")
        # v1.4.0 : on est passé de ".calendar_cache" à ".cache".
        # Si l'ancien dossier existe mais pas le nouveau, on le renomme
        # Ligne à supprimer plus tard
        if (not os.path.isdir(self.cache_directory) and
                os.path.isdir(os.path.join(pM.exe_dir, ".calendar_cache", ""))):
            os.rename(os.path.join(pM.exe_dir, ".calendar_cache", ""),
                      self.cache_directory)

        # Instanciation d'une fenêtre 'préférences' à partir du fichier de configuration de l'application.
        # Si ce fichier est absent :
        #  - il est reproduit depuis les ressources de l'application.
        #  - on considère que l'application est lancée pour la 1re fois (lancement ultérieur d'un 'wizard' ?)
        self.splash.showMessage("Récupération des préférences de l'application...", **self.splashMessageConfig)
        if not os.path.isfile(pM.settings_file):
            self.firstLaunch = True
            if QFile(pM.icon("ADEexplorer.ini")).copy(pM.settings_file):
                QFile(pM.settings_file).setPermissions(QFileDevice.ReadOwner | QFileDevice.WriteOwner)
        self.preferences = SettingDialog()
        if self.preferences.settings["gestion/url_root"].value == '':
            self.preferences.exec()

        # Lancement éventuel d'un "wizard 1re utilisation"
        if self.firstLaunch:
            ...

        # Création d'une checkBox pour (dés)activer l'affichage des agendas externes
        self.checkBox_display_extra_calendars = self.preferences.settings["Window/displayExtraCalendars"].widget
        self.checkBox_display_extra_calendars.setText('Afficher les agendas externes')
        font = QFont()
        font.setPointSize(8)
        self.checkBox_display_extra_calendars.setFont(font)
        self.checkBox_display_extra_calendars.setToolTip("<p>Intégrer les événements des agendas externes.</p>"
                                                         "<p>Pour associer un agenda externe : clic droit sur "
                                                         "la ressource > éditer.</p>")

        # Énumération des ressources
        self.splash.showMessage("Construction de l'arborescence des ressources", **self.splashMessageConfig)
        self.loadResources()

        # Téléchargement de tous les agendas selon les préférences
        self.splash.showMessage("Téléchargement des agendas des ressources...", **self.splashMessageConfig)
        if self.preferences.settings["gestion/telechargement/AllAtStartUp"].value:
            self.downloadAllAgendas()

        # Appel des fonctions d'initialisation de l'instance
        self.splash.showMessage("Initialisation de l'interface graphique...", **self.splashMessageConfig)
        self.initUI()
        # self.refreshTreeWidgetItemIcons()

        # Disparition du splashscreen
        self.splash.finish(self)
        self.splash.deleteLater()

        # Création d'un timer one-shot pour introduire une délai de réaction sur les textChanged() des filtres
        self.oneShotTimer = QTimer(self)
        self.oneShotTimer.setSingleShot(True)

        # Connexion des signaux aux slots
        self.connectSignals()

        # Activation d'un mode d'affichage par défaut
        self.action_affichageGraphique.setChecked(True)

        # Création et affichage d'un ADEcalendar (vide)
        self.oneAgenda = None
        self.updateAgendaView()

        # Comparaison de la version actuelle avec la version lancée précédemment (vient-on de faire une mise à jour ?)
        if self.preferences.settings["Version/version"].value != __ade_explorer_version__:
            self.oneShotTimer.timeout.connect(lambda: self.displayThisVersionNotes(fresh_update=True))
            self.oneShotTimer.start(200)
            self.preferences.settings["Version/version"].value = __ade_explorer_version__

        # Recherche silencieuse de mises à jour
        # Toute exception sera ignorée (mais affichée dans le terminal !)
        self.oneShotTimer.timeout.connect(lambda: self.tryUpdateCheck(silent=True))
        self.oneShotTimer.start(200)

        # Lancement de l'assistant à la migration vers une nouvelle année universitaire
        self.oneShotTimer.timeout.connect(lambda: self.new_year_wizard(auto_launch=True))
        self.oneShotTimer.start(500)

    def initUI(self):
        # Chargement de l'état de la fenêtre depuis les préférences
        self.restoreGeometry(self.preferences.settings["Window/geometry"].value)

        # Remise à zéro des filtres
        self.clearFilter()

        # Ajustement des dateEdit pour le filtre de période personnalisée
        self.dateEdit_FiltrePeriodeDebut.setDate(QDate.currentDate())
        self.dateEdit_FiltrePeriodeDebut.setMinimumDate(
            QDate(self.preferences.settings["annee/annee"].value.begin))
        self.dateEdit_FiltrePeriodeDebut.setMaximumDate(
            QDate(self.preferences.settings["annee/annee"].value.end))
        self.dateEdit_FiltrePeriodeDebut.setMinimumDate(
            QDate(self.preferences.settings["annee/annee"].value.begin))
        self.dateEdit_FiltrePeriodeFin.setMaximumDate(
            QDate(self.preferences.settings["annee/annee"].value.end))
        self.dateEdit_FiltrePeriodeFin.setDate(QDate.currentDate().addDays(28))

        # Ajout des actions à la barre d'outils
        self.toolBar.setIconSize(QSize(32, 32))

        self.actionGroup_mode = QActionGroup(self)
        self.actionGroup_mode.setExclusive(True)
        self.actionGroup_mode.addAction(self.action_modeSimple)
        self.actionGroup_mode.addAction(self.action_modeFreetime)
        self.actionGroup_mode.addAction(self.action_modeTogether)
        self.actionGroup_mode.setObjectName('actionGroup_mode')
        self.toolBar.addActions(self.actionGroup_mode.actions())
        self.action_modeSimple.setChecked(True)

        self.toolBar.addSeparator()

        self.actionGroup_affichage = QActionGroup(self)
        self.actionGroup_affichage.setExclusive(True)
        self.actionGroup_affichage.addAction(self.action_affichageGraphique)
        self.actionGroup_affichage.addAction(self.action_affichageListe)
        self.actionGroup_affichage.addAction(self.action_affichageStat)
        self.actionGroup_affichage.setObjectName('actionGroup_affichage')
        self.toolBar.addActions(self.actionGroup_affichage.actions())
        self.action_affichageGraphique.setChecked(True)

        self.toolBar.addSeparator()
        self.toolBar.addAction(self.action_agendaSnapshot)

        self.toolBar.addSeparator()
        self.toolBar.addAction(self.action_screenshot)

    def connectSignals(self):
        """
        Connecte les signaux aux slots.
        ATTENTION : les ResourceTreeView sont connectés dans 'self.loadResources()' car ils sont créés à la volée.
        """
        self.dateEdit_FiltrePeriodeDebut.dateChanged.connect(
            lambda: self.dateEdit_FiltrePeriodeFin.setMinimumDate(self.dateEdit_FiltrePeriodeDebut.date()))
        self.actionPlein_ecran.triggered.connect(lambda: (self.showFullScreen if self.actionPlein_ecran.isChecked()
                                                          else self.showNormal)())
        self.actionQuitter.triggered.connect(self.close)
        self.action_A_propos.triggered.connect(self.aboutDialog)
        self.action_notes_de_version.triggered.connect(self.displayThisVersionNotes)
        self.action_contact.triggered.connect(self.contactAuthor)
        self.action_help.triggered.connect(self.help)
        self.actionPreferences.triggered.connect(self.openPreferences)
        self.actionEditerListeRessources.triggered.connect(self.editRessourceFile)
        self.actionReload_resources.triggered.connect(self.loadResources)
        self.action_clearCache.triggered.connect(self.clearCache)
        self.action_update_check.triggered.connect(self.tryUpdateCheck)
        self.action_new_year_wizard.triggered.connect(self.new_year_wizard)

        self.actionAfficherDockRessources.triggered.connect(
            lambda: self.dockWidget_Ressources.setVisible(self.actionAfficherDockRessources.isChecked()))
        self.actionAfficherDockFiltres.triggered.connect(
            lambda: self.dockWidget_Filtres.setVisible(self.actionAfficherDockFiltres.isChecked()))
        self.action_addShortcut.triggered.connect(self.addShortcut)
        self.dockWidget_Ressources.visibilityChanged.connect(
            lambda: self.actionAfficherDockRessources.setChecked(self.dockWidget_Ressources.isVisible()))
        self.dockWidget_Filtres.visibilityChanged.connect(
            lambda: self.actionAfficherDockFiltres.setChecked(self.dockWidget_Filtres.isVisible()))

        self.checkBox_display_extra_calendars.toggled.connect(self.selectionRessourcesChanged)

        self.pushButton_FiltreClear.clicked.connect(self.clearFilter)
        for _signal in [self.actionGroup_mode.triggered,
                        self.actionGroup_affichage.triggered,
                        self.action_agendaSnapshot.triggered]:
            _signal.connect(self.updateAgendaView)

        self.action_screenshot.triggered.connect(
            lambda: takeScreenShot(self.centralwidget))

        for _signal in [self.radioButton_FiltrePeriodeS1.clicked,
                        self.radioButton_FiltrePeriodeS2.clicked,
                        self.radioButton_FiltrePeriodeAnnee.clicked,
                        self.radioButton_FiltrePeriodeFin.clicked,
                        self.radioButton_FiltrePeriodeAutre.clicked,
                        self.dateEdit_FiltrePeriodeDebut.dateChanged,
                        self.dateEdit_FiltrePeriodeFin.dateChanged,
                        self.comboBox_FiltreContenuIncludeLogical.currentIndexChanged,
                        self.comboBox_FiltreContenuExcludeLogical.currentIndexChanged,
                        self.comboBox_FiltreParticipantsIncludeLogical.currentIndexChanged,
                        self.comboBox_FiltreParticipantsExcludeLogical.currentIndexChanged,
                        self.comboBox_FiltreLieuIncludeLogical.currentIndexChanged,
                        self.comboBox_FiltreLieuExcludeLogical.currentIndexChanged,
                        self.timeEdit_FiltreDuree.timeChanged,
                        self.comboBox_FiltreDureeLogical.currentIndexChanged,
                        self.checkBox_FiltreJourLundi.toggled,
                        self.checkBox_FiltreJourMardi.toggled,
                        self.checkBox_FiltreJourMercredi.toggled,
                        self.checkBox_FiltreJourJeudi.toggled,
                        self.checkBox_FiltreJourVendredi.toggled,
                        self.checkBox_FiltreJourSamedi.toggled]:
            # La connexion à un oneShotTimer permet d'éviter plusieurs accès successifs lors de
            # l'appel à la fonction 'self.clearFilter()'
            _signal.connect(lambda: self.slotWithDelay(slot=lambda: self.updateAgendaView(), delay=10))

        for _signal in [self.lineEdit_FiltreContenuInclude.textChanged,
                        self.lineEdit_FiltreContenuExclude.textChanged,
                        self.lineEdit_FiltreParticipantsInclude.textChanged,
                        self.lineEdit_FiltreParticipantsExclude.textChanged,
                        self.lineEdit_FiltreLieuInclude.textChanged,
                        self.lineEdit_FiltreLieuExclude.textChanged]:
            # La connexion à un oneShotTimer permet d'éviter le re-calcul de l'agenda pour chaque caractère
            # modifié par l'utilisateur. Ici, il y a un temps de latence de 500 ms.
            _signal.connect(lambda: self.slotWithDelay(slot=lambda: self.updateAgendaView(), delay=500))

    @pyqtSlot()
    def slotWithDelay(self, slot: callable, delay: int = 500):
        try:
            self.oneShotTimer.timeout.disconnect()
        except TypeError:
            pass
        self.oneShotTimer.timeout.connect(slot)
        self.oneShotTimer.start(delay)

    def close(self) -> bool:
        self.preferences.settings["Window/geometry"].value = self.saveGeometry()
        self.preferences.settings["Window/state"].value = self.saveState()
        QApplication.closeAllWindows()
        return super(MainWindow, self).close()

    def closeEvent(self, a0: QCloseEvent) -> None:
        self.close()

    def addShortcut(self):
        if sys.platform == 'linux':
            _type = 'Application'
            _name = f'ADEexplorer v{__ade_explorer_version__}'
            if hasattr(sys, 'frozen'):
                _startupWMClass = 'ADEexplorer'
            else:
                _startupWMClass = 'ADEexplorer.py'
            _comment = "Facilite la consultation et l'analyse des emplois du temps ADE."
            if pM.exe_file == sys.executable:
                # Version empaquetée avec pyInstaller
                _exec = f'"{sys.executable}" '
                _icon = os.path.join(pM.exe_dir, 'ADEicon.png')
            else:
                # version 'code source'
                _exec = f'"{sys.executable}" "{pM.exe_file}"'
                _icon = os.path.join(pM.exe_dir, 'resources/icons/ADEicon.png')
            _terminal = 'false'
            desktop_file = os.path.join(QStandardPaths.standardLocations(QStandardPaths.ApplicationsLocation)[0],
                                        f'ADEexplorer.desktop')

            reply = QMessageBox.question(self, "Ajout d'un raccourci",
                                         f"Voulez-vous créer le fichier intitulé<br>"
                                         f"<tt>{desktop_file}</tt> ?<br>",
                                         QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
            if reply == QMessageBox.StandardButton.No:
                return

            # dans sa version 1.3.0, ADEexplorer incluait le n° de version dans le nom du fichier desktop.
            # On supprime ce raccourci s'il existe.
            try:
                os.remove(os.path.join(QStandardPaths.standardLocations(QStandardPaths.ApplicationsLocation)[0],
                                       f'ADEexplorer_v1.3.0.desktop'))
            except OSError:
                pass

            with open(desktop_file, 'w+') as file:
                file.write(f"[Desktop Entry]\n"
                           f"Name={_name}\n"
                           f"Type={_type}\n"
                           f"Comment={_comment}\n"
                           f"Terminal={_terminal}\n"
                           f"Icon={_icon}\n"
                           f"Exec={_exec}\n"
                           f"StartupWMClass={_startupWMClass}")

            reply = QMessageBox.information(self, "Ajout d'un raccourci",
                                            f"Le fichier a bien été ajouté.<br>"
                                            f"Après quelques secondes, l'application doit être disponible. "
                                            f"Dans le cas contraire, lancer la commande :<br>"
                                            f"<tt>desktop-file-validate {desktop_file}</tt> puis corriger le fichier "
                                            f"en conséquence.",
                                            QMessageBox.Ok)

        if sys.platform == 'win32':
            from win32com.client import Dispatch
            reply = QMessageBox.question(self, "Ajout d'un raccourci",
                                         f"Vous-êtes sur le point d'ajouter un raccourci vers ADEexplorer "
                                         f"sur le bureau et dans le menu démarrer."
                                         f"Voulez-vous acontinuer ?",
                                         QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
            if reply == QMessageBox.StandardButton.No:
                return
            for destination in (
                    QStandardPaths.standardLocations(QStandardPaths.DesktopLocation)[0],  # Bureau
                    QStandardPaths.standardLocations(QStandardPaths.ApplicationsLocation)[0]  # Menu démarrer
            ):
                shell = Dispatch('WScript.Shell')
                shortcut = shell.CreateShortCut(os.path.join(destination, "ADEexplorer.lnk"))
                shortcut.Targetpath = sys.executable
                icon = pM.exe_file
                if pM.exe_file != sys.executable:
                    shortcut.Arguments = pM.exe_file
                    icon = os.path.join(pM.exe_dir, 'ADEicon.ico')
                shortcut.WorkingDirectory = pM.exe_file
                shortcut.IconLocation = icon
                shortcut.save()

        if sys.platform == 'darwin':
            reply = QMessageBox.information(self, "Ajout d'un raccourci",
                                            f"Cette fonctionnalité n'est pas encore disponible sous MacOS.",
                                            QMessageBox.Ok)

    def tryUpdateCheck(self, silent: bool = False):
        try:
            self.updateCheck(silent=silent)
        except Exception as e:
            if silent:
                print(type(e))
            else:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Icon.Critical)
                msg.setWindowTitle("Erreur")
                msg.setText(f"La recherche de mise à jour a échoué")
                msg.setDetailedText(f"{e}")
                msg.exec()

    def updateCheck(self, silent: bool = False):
        if silent and not self.preferences.settings["updates/auto"].value:
            return

        # Mise à jour uniquement pour les versions bundled
        if not hasattr(sys, 'frozen'):
            if not silent:
                QMessageBox.information(self, "Recherche de mises à jour...",
                                        "La mise à jour automatique est possible uniquement pour "
                                        "les versions exécutables d'ADEexplorer.", QMessageBox.StandardButton.Ok)
            return

        suffix_old = '@@@OLD@@@'
        suffix_new = '@@@NEW@@@'
        old_files_to_remove = []
        temp_files_to_remove = os.path.join(pM.exe_dir, '.to_be_removed')

        if os.path.isfile(temp_files_to_remove):
            with open(temp_files_to_remove, 'r') as f:
                for line in f.readlines():
                    try:
                        os.remove(line.strip())
                    except OSError:
                        pass
            try:
                os.remove(temp_files_to_remove)
            except OSError:
                pass

        # Construction de l'url de téléchargement du fichier 'latest_release.txt' qui contient les information
        # sur la dernière 'release' pour chaque plateforme, notamment la liste des fichiers à télécharger
        try:
            seafile_url = requests.get(__gitlab__ + "-/raw/master/binaries").content.decode('utf8')
        except requests.exceptions.RequestException:
            raise
        seafile_url_parseResult = urllib.parse.urlparse(seafile_url)
        seafile_url_parseResult = seafile_url_parseResult._replace(
            query=urllib.parse.urlencode({'p': '/latest_release.txt', 'dl': 1}))
        latest_release_url = seafile_url_parseResult.geturl()
        try:
            latest_release_content = requests.get(latest_release_url, timeout=2).content.decode(encoding='utf8',
                                                                                                errors='replace')
        except requests.exceptions.RequestException:
            raise
        platform = ''
        version = ''
        server_path = ''
        remote_files = []
        for _line in latest_release_content.splitlines():
            try:
                _p, _v, _sp, *_f = _line.split(',')
            except ValueError:
                continue
            if _p == sys.platform or (_p == sys.platform + '_dev' and self.preferences.settings["updates/dev"].value):
                platform, version, server_path, remote_files = _p.strip(), _v.strip(), _sp.strip(), _f
        if not platform:
            if not silent:
                QMessageBox.information(self, "Recherche de mises à jour...",
                                        f"Aucune version d'ADEexplorer n'est disponible pour "
                                        f"la plateforme {sys.platform}", QMessageBox.StandardButton.Ok)
            return

        if not tuple(map(int, (version.split(".")))) > tuple(map(int, (__ade_explorer_version__.split(".")))):
            if not silent:
                QMessageBox.information(self, "Recherche de mises à jour...",
                                        f"Vous possédez la version la plus récente d'ADEexplorer "
                                        f"({__ade_explorer_version__}).")
            return

        # Recherche et affichage d'un éventuel fichier 'version_notes.md'
        seafile_url_parseResult = seafile_url_parseResult._replace(
            query=urllib.parse.urlencode({'p': posixpath.join(server_path, 'version_notes.md'), 'dl': 1}))
        try:
            r = requests.get(seafile_url_parseResult.geturl())
        except requests.exceptions.RequestException:
            raise
        if r.headers.get('content-length') is not None:
            version_notes = r.content.decode('utf8')
        else:
            version_notes = "Aucune note de version n'a été trouvée sur le serveur."

        msg = VersionNotes()
        msg.setText(f"Une nouvelle version d'ADEexplorer est disponible en ligne (version {version}).")
        msg.setVersionNote(version_notes)
        msg.setInformativeText(f"Voulez-vous la télécharger ?")
        msg.setWindowTitle("Recherche de mises à jour...")
        msg.buttonBox.setStandardButtons(QDialogButtonBox.StandardButton.Yes | QDialogButtonBox.StandardButton.No)
        msg.buttonBox.button(QDialogButtonBox.StandardButton.No).setDefault(True)
        msg.checkBox.setText("Activer la recherche automatique de mises à jour.")
        msg.checkBox.setChecked(self.preferences.settings["updates/auto"].value)
        reply = msg.exec()
        self.preferences.settings["updates/auto"].value = msg.checkBox.isChecked()
        if reply != QDialog.DialogCode.Accepted:
            return

        # Création d'un dialogue avec barre de progression
        progress = QProgressDialog("Téléchargement de la mise à jour", "Annuler", 0, 0, self)
        progress.setWindowTitle("Recherche de mises à jour...")
        progress.setWindowModality(Qt.WindowModality.WindowModal)
        progress.setWindowIcon(QIcon(pM.icon("downlaod.svg")))

        # Téléchargement des fichiers, un par un
        for remote_file in remote_files:
            remote_file = remote_file.strip()
            # Construction de l'url de téléchargement
            seafile_url_parseResult = seafile_url_parseResult._replace(
                query=urllib.parse.urlencode({'p': posixpath.join(server_path, remote_file), 'dl': 1}))
            remote_file_url = seafile_url_parseResult.geturl()

            # Construction des chemins d'accès au (futur) fichier
            local_file_path = os.path.join(pM.exe_dir, remote_file)
            local_file_path_new = local_file_path + suffix_new
            local_file_path_old = local_file_path + suffix_old

            # Requête de téléchargement lancée...
            try:
                with requests.get(remote_file_url, stream=True) as r:
                    # Si cet attribut est None, le fichier n'existe pas, on passe au suivant
                    if not r.headers.get('content-length'):
                        continue

                    progress.setMaximum(int(r.headers['Content-length']))
                    r.raise_for_status()

                    # Si le répertoire cible du fichier téléchargé n'existe pas
                    # on essaie de le créer
                    if not os.path.exists(os.path.dirname(local_file_path_new)):
                        try:
                            os.makedirs(os.path.dirname(local_file_path_new))
                        except OSError as exc:  # Guard against race condition
                            if exc.errno != errno.EEXIST:
                                raise

                    # On ouvre le fichier pour écriture (binaire)...
                    with open(local_file_path_new, 'wb') as f:
                        i = 0
                        chunk_size = 8192
                        for chunk in r.iter_content(chunk_size=chunk_size):
                            f.write(chunk)
                            i += 1
                            progress.setValue(i * chunk_size)
                            if progress.wasCanceled():
                                QMessageBox.warning(self, "Recherche de mises à jour...",
                                                    "Téléchargement annulé.")
                                return

                    if hasattr(os, 'chmod'):
                        os.chmod(local_file_path_new, 0o775)
            except requests.exceptions.RequestException:
                raise

            if os.path.isfile(local_file_path):
                os.rename(local_file_path, local_file_path_old)
                old_files_to_remove.append(local_file_path_old)
            os.rename(local_file_path_new, local_file_path)

        progress.close()

        with open(temp_files_to_remove, 'w') as f:
            f.write('\n'.join(old_files_to_remove) + '\n')
        QMessageBox.information(self, "Mise à jour effectuée",
                                f"La nouvelle version d'ADEexplorer a bien été installée.<br>"
                                f"L'application va se fermer puis se relancer automatiquement.")

        subprocess.Popen(pM.exe_file, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
        QCoreApplication.quit()
        sys.exit(0)

    def aboutDialog(self):
        caption = "À propos d'ADEexplorer"
        text = f"""
        <p><span style=" font-size:14pt; font-weight:600;">ADEexplorer</span></p>
        <p>ADEexplorer facilite la consultation et l'analyse<br>
        des emplois du temps ADE.</p>
        <p>
        <table rules="rows">
        <tr>
            <td>Version</td>
            <td>: {__ade_explorer_version__} ({sys.platform.capitalize()})</td>
        </tr>
        <tr>
            <td>Auteur</td>
            <td>: Vincent Raspal</td>
        </tr>
        <tr>
            <td>Mail</td>
            <td>: <a href='mailto:vincent.raspal@uca.fr?subject=[ADEexplorer_v{__ade_explorer_version__} ({sys.platform.capitalize()})]'>
                vincent.raspal@uca.fr</a></td>
        </tr>
        <tr>
            <td>Dépôt en ligne </td>
            <td>: <a href='{__gitlab__}'>gitlab.com</a></td>
        </tr>
        <tr>
            <td>Licence </td>
            <td>: <a href='https://www.gnu.org/licenses/gpl-3.0.html'>GNU GPLv3</a></td>
        </tr>
        </table>
        </p>
        """

        QMessageBox(QMessageBox.about(self, caption, text))

    def displayThisVersionNotes(self, fresh_update: bool = False):
        try:
            with open(os.path.join(pM.exe_dir, 'version_notes.md'), 'r', encoding='utf8') as f:
                version_notes = f.read()
        except OSError:
            QMessageBox.critical(self, "Erreur", "Le fichier <tt>version_notes.md</tt> n'a pas pu être trouvé.",
                                 QMessageBox.StandardButton.Ok)
            return
        msg = VersionNotes()
        msg.setVersionNote(version_notes)

        if fresh_update:
            msg.setText("Félicitations ! La mise à jour d'ADEexplorer s'est déroulée avec succès.")
            msg.setInformativeText(f"Ci-dessous les notes de la version {__ade_explorer_version__}. "
                                   f"Vous pourrez les consulter plus tard via le menu "
                                   f"<tt>Aide > Notes de version</tt>.")
        else:
            msg.text.hide()
            msg.informativeText.hide()

        msg.setWindowTitle("Notes de version")
        msg.buttonBox.setStandardButtons(QDialogButtonBox.StandardButton.Ok)
        msg.buttonBox.button(QDialogButtonBox.StandardButton.Ok).setDefault(True)
        msg.checkBox.hide()
        reply = msg.exec()

    def clearCache(self):
        for f in os.listdir(self.cache_directory):
            if os.path.splitext(f)[1] != '.ics':
                continue
            try:
                os.remove(os.path.join(self.cache_directory, f))
            except OSError as e:
                print(f"Error: {self.cache_directory} : {e.strerror}")
        self.loadResources()

    def loadResources(self):
        self.resource_file = os.path.join(pM.exe_dir, self.preferences.settings["ressources/file"].value)
        if not os.path.isfile(self.resource_file):
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Icon.Warning)

            msg.setText(f"Le fichier <tt>{self.preferences.settings['ressources/file'].value}</tt> est introuvable.")
            msg.setDetailedText("Le chemin d'accès connu pour le fichier est le suivant :\n"
                                f"{self.resource_file}")
            msg.setInformativeText(f"Si le fichier <b>existe</b> sous un autre nom, "
                                   f"<ul><li>aller dans le menu "
                                   f"<tt>Édition > Préférences</tt>,</li>"
                                   f"<li>aller dans l'onglet <tt>Ressources</tt> pour modifier le nom.</li></ul>"
                                   f"Si le fichier <b>n'existe pas</b>, ADEexplorer peut créer un fichier vierge "
                                   f"sous ce nom.")
            msg.setWindowTitle("Fichier ressources introuvable")
            msg.setStandardButtons(QMessageBox.StandardButton.Retry | QMessageBox.StandardButton.Ok)
            layout: QLayout = msg.layout()
            layout.addItem(QSpacerItem(700, 0, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding),
                           layout.rowCount(), 0, 1, layout.columnCount())

            button_R = msg.button(QMessageBox.StandardButton.Retry)
            button_R.setText('ADEexplorer doit créer le fichier vierge')
            button_R.setIcon(QIcon(""))
            button_O = msg.button(QMessageBox.StandardButton.Ok)
            button_O.setText("Le fichier existe, je vais corriger son chemin d'accès")
            button_O.setIcon(QIcon(""))

            retval = msg.exec()
            if retval == QMessageBox.StandardButton.Retry:
                self.resource_file = os.path.join(pM.exe_dir, "RESSOURCES.csv")
                ResourceTreeStructure.create_empty_file(self.resource_file or re,
                                                        categories=['Collègues', 'Groupes', 'Salles'],
                                                        delimiter=self.preferences.settings[
                                                                      "ressources/separator"].value or ',',
                                                        encoding='utf8')
                self.preferences.settings["ressources/file"].value = self.resource_file
            else:
                return

        self.resourceTreeStructure = ResourceTreeStructure(file=self.resource_file,
                                                           delimiter=self.preferences.settings[
                                                               "ressources/separator"].value)
        for w in self.scrollAreaWidgetContents_Ressources.children():
            if isinstance(w, ResourceTreeView):
                w.deleteLater()
        self.resourceTreeModels.clear()
        self.resourceTreeViews.clear()
        self.ressources_verticalLayout.addWidget(self.checkBox_display_extra_calendars)
        try:
            with open(pM.treeview_expansion_file, 'r') as f:
                expanded = json.load(f)
        except (FileNotFoundError, json.decoder.JSONDecodeError):
            expanded = list()
        for resourceNode in self.resourceTreeStructure.categories:
            model = ResourceTreeModel(resourceNode)
            treeview = ResourceTreeView(self.scrollAreaWidgetContents_Ressources, parent=self)
            treeview.setModel(model)
            treeview.expandNodes(expanded)
            treeview.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)
            self.ressources_verticalLayout.addWidget(treeview)
            self.resourceTreeModels.append(model)
            self.resourceTreeViews.append(treeview)
            treeview.itemSelectionChanged.connect(
                lambda: self.slotWithDelay(slot=lambda: self.selectionRessourcesChanged(), delay=10))
            treeview.forceDownloadRequired.connect(lambda node: self.loadCalendar(node, force_download=True))
            treeview.showCollisions.connect(lambda node: self.showCollisions(node, force_window=True))
            treeview.resourceTreeStructureChanged.connect(self.resourceTreeStructure.write_file)
            treeview.nodeEdited.connect(self.showHideExtraCalendarsCheckbox)
            treeview.nodeExpansionChanged.connect(self.saveTreeviewExpansionState)
        self.showHideExtraCalendarsCheckbox()
        for tree1 in self.resourceTreeViews:
            for tree2 in self.resourceTreeViews:
                if tree1 is not tree2:
                    tree1.clearOtherTreeViewSelection.connect(tree2.clearSelection)

    @pyqtSlot()
    def showHideExtraCalendarsCheckbox(self):
        show = any((model.has_extra_url for model in self.resourceTreeModels))
        self.checkBox_display_extra_calendars.setEnabled(show)

    @pyqtSlot()
    def editRessourceFile(self):
        url = QUrl.fromLocalFile(os.path.join(pM.exe_dir, self.preferences.settings["ressources/file"].value))
        QDesktopServices.openUrl(url)
        response = QMessageBox.information(self, "Édition de la liste des ressources",
                                           "Le fichier contenant la liste des ressources va s'ouvrir avec "
                                           "l'application par défaut associée aux fichiers csv.\n"
                                           "Sélectionnez le bon format d'encodage (Unicode UTF-8) et le bon séparateur "
                                           "de colonnes (,).\n"
                                           "Une fois le fichier modifié et enregistré, cliquez sur 'Yes' pour "
                                           "recharger les ressources dans ADEexplorer.",
                                           QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.Yes,
                                           QMessageBox.StandardButton.Cancel)
        if response == QMessageBox.StandardButton.Cancel:
            return
        elif response == QMessageBox.StandardButton.Yes:
            self.loadResources()

    def updateFilter(self, sender=None):
        if sender not in [self.oneShotTimer, None]:
            return

        # Filtre sur la plage calendaire
        self.oneAgenda_event_filter.setDateRange(self.oneAgenda_begin_date, self.oneAgenda_end_date)

        # Filtre sur les jours de la semaine
        self.oneAgenda_event_filter.setWeekdays([i for i, w in enumerate([
            self.checkBox_FiltreJourLundi, self.checkBox_FiltreJourMardi,
            self.checkBox_FiltreJourMercredi, self.checkBox_FiltreJourJeudi,
            self.checkBox_FiltreJourVendredi, self.checkBox_FiltreJourSamedi
        ]) if w.isChecked()])

        # Filtre sur le contenu in/ex-clusion
        if self.comboBox_FiltreContenuIncludeLogical.currentText().lower() in ['all', 'any']:
            _logical_include = self.comboBox_FiltreContenuIncludeLogical.currentText().lower()
        else:
            _logical_include = ['all']

        if self.comboBox_FiltreContenuExcludeLogical.currentText().lower() in ['all', 'any']:
            _logical_exclude = self.comboBox_FiltreContenuExcludeLogical.currentText().lower()
        else:
            _logical_exclude = ['all']
        self.oneAgenda_event_filter.setContentInclude(self.lineEdit_FiltreContenuInclude.text(), _logical_include)
        self.oneAgenda_event_filter.setContentExclude(self.lineEdit_FiltreContenuExclude.text(), _logical_exclude)

        # Filtre sur les participants in/ex-clusion
        if self.comboBox_FiltreParticipantsIncludeLogical.currentText().lower() in ['all', 'any']:
            _logical_include = self.comboBox_FiltreParticipantsIncludeLogical.currentText().lower()
        else:
            _logical_include = ['all']

        if self.comboBox_FiltreParticipantsExcludeLogical.currentText().lower() in ['all', 'any']:
            _logical_exclude = self.comboBox_FiltreParticipantsExcludeLogical.currentText().lower()
        else:
            _logical_exclude = ['all']
        self.oneAgenda_event_filter.setParticipantsInclude(self.lineEdit_FiltreParticipantsInclude.text(),
                                                           _logical_include)
        self.oneAgenda_event_filter.setParticipantsExclude(self.lineEdit_FiltreParticipantsExclude.text(),
                                                           _logical_exclude)

        # Filtre sur le lieu in/ex-clusion
        if self.comboBox_FiltreLieuIncludeLogical.currentText().lower() in ['all', 'any']:
            _logical_include = self.comboBox_FiltreLieuIncludeLogical.currentText().lower()
        else:
            _logical_include = ['all']

        if self.comboBox_FiltreLieuExcludeLogical.currentText().lower() in ['all', 'any']:
            _logical_exclude = self.comboBox_FiltreLieuExcludeLogical.currentText().lower()
        else:
            _logical_exclude = ['all']
        self.oneAgenda_event_filter.setLieuInclude(self.lineEdit_FiltreLieuInclude.text(), _logical_include)
        self.oneAgenda_event_filter.setLieuExclude(self.lineEdit_FiltreLieuExclude.text(), _logical_exclude)

        # Filtre sur la durée
        if self.comboBox_FiltreDureeLogical.currentText().lower() in ['>=', '=>', '≥']:
            _logical_duration = '>='
        elif self.comboBox_FiltreDureeLogical.currentText().lower() in ['<=', '=<', '≤']:
            _logical_duration = '<='
        elif self.comboBox_FiltreDureeLogical.currentText().lower() in ['>', '<', '=']:
            _logical_duration = self.comboBox_FiltreDureeLogical.currentText().lower()
        else:
            _logical_duration = '>='
        _duration = timedelta(milliseconds=self.timeEdit_FiltreDuree.time().msecsSinceStartOfDay())
        self.oneAgenda_event_filter.setDuration(_duration, _logical_duration)

    def clearFilter(self, do_not_clear_period: bool = False):
        # La période activée est celle contenant la date du jour.
        # En cas de non-correspondance, l'année complète est activée par défaut
        if not do_not_clear_period:
            self.radioButton_FiltrePeriodeAnnee.setChecked(True)
            today = arrow.Arrow.utcnow().date()
            S1_debut = self.preferences.settings['annee/semestre1'].value.begin_arrow.date()
            S1_fin = self.preferences.settings['annee/semestre1'].value.end_arrow.date()
            S2_debut = self.preferences.settings['annee/semestre2'].value.begin_arrow.date()
            S2_fin = self.preferences.settings['annee/semestre2'].value.end_arrow.date()

            self.radioButton_FiltrePeriodeS1.setChecked(S1_debut <= today <= S1_fin)
            self.radioButton_FiltrePeriodeS2.setChecked(S2_debut <= today <= S2_fin)

        # Par défaut, le filtre de Durée est positionné sur '≥ 00:00'
        self.comboBox_FiltreDureeLogical.setCurrentIndex(0)
        self.timeEdit_FiltreDuree.setTime(QTime(0, 0))

        # Les filtres de contenus sont vidés et remis sur 'any'
        self.lineEdit_FiltreContenuInclude.clear()
        self.lineEdit_FiltreContenuExclude.clear()
        self.comboBox_FiltreContenuIncludeLogical.setCurrentText('all')
        self.comboBox_FiltreContenuExcludeLogical.setCurrentText('any')
        self.lineEdit_FiltreParticipantsInclude.clear()
        self.lineEdit_FiltreParticipantsExclude.clear()
        self.comboBox_FiltreParticipantsIncludeLogical.setCurrentText('all')
        self.comboBox_FiltreParticipantsExcludeLogical.setCurrentText('any')
        self.lineEdit_FiltreLieuInclude.clear()
        self.lineEdit_FiltreLieuExclude.clear()
        self.comboBox_FiltreLieuIncludeLogical.setCurrentText('all')
        self.comboBox_FiltreLieuExcludeLogical.setCurrentText('any')

        # Les jours de la semaine sont tous cochés
        self.checkBox_FiltreJourLundi.setChecked(True)
        self.checkBox_FiltreJourMardi.setChecked(True)
        self.checkBox_FiltreJourMercredi.setChecked(True)
        self.checkBox_FiltreJourJeudi.setChecked(True)
        self.checkBox_FiltreJourVendredi.setChecked(True)
        self.checkBox_FiltreJourSamedi.setChecked(True)

    @pyqtSlot()
    def selectionRessourcesChanged(self):
        """
        Passe en revue tous les ResourceNode de l'arborescence (self.resourceTreeStructure)
        en cherchant récursivement tous les enfants à partir  de la racine (.root_item).
        Pour chacune des ressources (pas les conteneurs !), applique self.refreshCalendar()
        Termine par une mise à jour de la vue des agendas.
        """
        for node in PreOrderIter(self.resourceTreeStructure.root_item):
            assert isinstance(node, ResourceNode)
            if node.isResource() and node.isSelected and (not node.calendar or
                                                          node.calendar.dataStatus & DataStatus.noData or
                                                          node.calendar.fileIsTooOld()):
                try:
                    self.loadCalendar(node)
                    self.showCollisions(node)
                except MissingCalendarError:
                    pass
        self.updateAgendaView()

    @pyqtSlot()
    def saveTreeviewExpansionState(self):
        expanded = []
        for treeview in self.resourceTreeViews:
            expanded += treeview.expandedNodes
        with open(pM.treeview_expansion_file, 'w+', encoding='utf8') as f:
            f.writelines(json.dumps(sorted(expanded), ensure_ascii=False, indent=4))

    def downloadAllAgendas(self):
        """
        Télécharge les agendas de toutes les ressources.
        """
        for node in PreOrderIter(self.resourceTreeStructure.root_item):
            assert isinstance(node, ResourceNode)
            if node.isResource():
                try:
                    self.loadCalendar(node)
                except MissingCalendarError:
                    pass

    def loadCalendar(self, node: ResourceNode, force_download: bool = False):
        """
        Effectue une mise à jour de l'ADEcalendar stocké dans l'attribut '.calendar' de la ressource
        'ressourceNode'. Selon les règles de péremption et l'âge du fichier ics éventuellement présent
        en cache sur le disque, un nouveau fichier ics sera ou non téléchargé avant son traitement.
        Voir ADEcalendar() pour plus de détails.
        Le statut de l'agenda est mis à jour (ressourceNode.calendar.dataStatus). En cas d'erreur (.noData,
        .failedDownload ou .failedReadCache), une fenêtre d'information est affichée.

        """
        uid = node.uid
        title = node.name
        # Le pointeur de la souris est mis en mode 'Patienter'
        QApplication.setOverrideCursor(Qt.CursorShape.WaitCursor)
        # Les agendas seront téléchargés sur une plage calendaire égale à l'année universitaire
        # (telle que définie par l'utilisateur dans les 'Préférences').
        _debut = self.preferences.settings["annee/annee"].value.begin_arrow
        _fin = self.preferences.settings["annee/annee"].value.end_arrow
        # Un message de chargement de l'agenda est affiché, soit dans le splashscreen s'il existe,
        # soit dans la barre de statut.
        try:
            self.splash.showMessage(f"Chargement de la ressource {title}...", **self.splashMessageConfig)
        except RuntimeError:
            self.statusBar.showMessage(f"Chargement de la ressource {title}...", 2000)
        QCoreApplication.processEvents()

        # Gestion de la péremption
        if force_download:
            peremption = 0
        else:
            peremption = self.preferences.settings["gestion/peremption"].value

        # Demande de (télé)chargement de l'ics pour créer un ADEcalendar
        node.calendar = ADEcalendar(ressource_id=uid,
                                    url_root=self.preferences.settings["gestion/url_root"].value,
                                    project_id=self.preferences.settings["gestion/projectID"].value,
                                    first_date=_debut, last_date=_fin,
                                    peremption=peremption,
                                    cache_directory=self.cache_directory)
        node.calendar.setTitle(title)
        if node.extra_url:
            node.merged_calendar = deepcopy(node.calendar)
            node.extra_calendars.clear()
            extra_agendas_data = ExtraAgendasData(node.extra_url)
            for i, extra_agenda_data in enumerate(extra_agendas_data.extra_agendas):
                extra_calendar = ADEcalendar(ressource_id=uid,
                                             url_root=extra_agenda_data.url,
                                             first_date=_debut, last_date=_fin,
                                             peremption=peremption,
                                             cache_directory=self.cache_directory,
                                             extra_provider=True,
                                             credentials=extra_agenda_data.credentials,
                                             color=extra_agenda_data.color,
                                             name=extra_agenda_data.name)
                node.extra_calendars.append(extra_calendar)
                extra_calendar.setTitle(title)
                node.merged_calendar.events = set.union(node.merged_calendar.events, extra_calendar.events)
            node.merged_calendar.fillSortedEventDictionaries()
        node.updateAge()

        # Gestion et affichage des erreurs ayant eu lieu lors de la création du ADEcalendar
        #   1. Pas de données du tout (pas de cache, téléchargement impossible)
        if node.calendar.dataStatus & DataStatus.noData and not self.preferences.settings["gestion/no_warning"].value:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Icon.Critical)
            msg.setWindowTitle("Échec lors du chargement")
            msg.setText(f"<p>La ressource {title} n'a pu être téléchargée "
                        f"et aucune version locale n'a été trouvée.</p>"
                        f"<p>ADEexplorer retentera le téléchargement la prochaine fois que la ressource "
                        f"sera sélectionnée.</p>"
                        f"<p> Si le problème persiste, cela provient généralement :"
                        f"<ul>"
                        f"<li>d'une mauvaise connexion ;</li>"
                        f"<li>du serveur ADE qui ne répond pas (généralement temporaire).</li>"
                        f"</ul></p>")
            msg.setDetailedText(node.calendar.errorMessage)
            cb = QCheckBox("Ne plus afficher de message en cas d'erreur de téléchargement.")
            cb.setChecked(self.preferences.settings["gestion/no_warning"].value)
            msg.setCheckBox(cb)
            msg.exec()
            self.preferences.settings["gestion/no_warning"].value = cb.isChecked()
            QApplication.restoreOverrideCursor()
            # raise MissingCalendarError

        #   2. Téléchargement impossible, cache chargé
        if (node.calendar.dataStatus & DataStatus.failedDownload
                and not self.preferences.settings["gestion/no_warning"].value):
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Icon.Warning)
            msg.setWindowTitle("Erreur lors du téléchargement...")
            msg.setText(f"<p>La ressource <b>'{title}'</b> n'a pas pu être téléchargée. "
                        f"La version locale a été chargée à la place.</p>"
                        f"<p>Âge de la version locale : {ResourceNode.formatAge(node.age)}</p>"
                        f"<p>Vous pouvez tenter à nouveau le téléchargement par un clic droit sur la ressource.</p>")
            msg.setDetailedText(node.calendar.errorMessage)
            msg.setDefaultButton(QMessageBox.Ok)
            msg.setDetailedText(node.calendar.errorMessage)
            cb = QCheckBox("Ne plus afficher de message en cas d'erreur de téléchargement.")
            cb.setChecked(self.preferences.settings["gestion/no_warning"].value)
            msg.setCheckBox(cb)
            msg.exec()
            self.preferences.settings["gestion/no_warning"].value = cb.isChecked()

        #   3. Téléchargement cache corrompu alors que le téléchargement n'était pas nécessaire.
        if node.calendar.dataStatus & DataStatus.corruptedCache:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Icon.Warning)
            msg.setWindowTitle("Erreur lors de la lecture du fichier local...")
            msg.setText(f"<p>La version locale de l'agenda est corrompue. "
                        f"<p>Vous pouvez forcer le téléchargement de l'agenda par un clic droit sur la ressource.</p>")
            msg.setDetailedText(node.calendar.errorMessage)
            msg.exec()

        QApplication.restoreOverrideCursor()
        for model in self.resourceTreeModels:
            model.dataChanged.emit(QModelIndex(), QModelIndex())
        if force_download:
            self.selectionRessourcesChanged()

    def openPreferences(self, cat: str = ""):
        """
        Ouvre la fenêtre des préférences, puis remet à jour l'affichage des agendas.
        """
        _resource_file = self.preferences.settings["ressources/file"].value
        rep = self.preferences.exec(cat)
        if rep == QDialog.DialogCode.Rejected:
            return
        # Si self.preferences.ade_settings["ressources/file"].value a changé lors de self.preferences.exec()
        # alors on recharge les ressources.
        if _resource_file != self.preferences.settings["ressources/file"].value:
            self.loadResources()
        self.updateAgendaView()

    def holidays(self, begin: arrow.Arrow, end: arrow.Arrow):
        """
        Renvoie la liste des vacances et jours fériés compris entre 'begin' et 'end'.
        """
        dates: list[arrow.Arrow] = list()
        # Les dates non travaillées sont de 2 types :
        #  -- des vacances définies par des tupes (début, fin) qu'il convient de transformer en liste de dates contiguës
        for debut, fin in [(self.preferences.settings[key].value.begin_arrow,
                            self.preferences.settings[key].value.end_arrow) for key in self.preferences.settings.keys()
                           if key.startswith('vacances/')]:
            dates.extend(arrow.Arrow.range('day', debut.replace(tzinfo=ADEcalendar.TimeZone),
                                           fin.replace(tzinfo=ADEcalendar.TimeZone)))
        # -- des dates de jours fériés
        dates.extend(
            joursFeriesFrancais.holiday_days(begin, end, lundiPentecote=True, lundiPaques=True,
                                             vendrediAscension=True))
        return dates

    def computeAgendaParameters(self, sender):
        """
        Récupération de toutes les préférences d'affichage des agendas et mise à jour des variables associées.
        Ces variables mises à jour sont toutes du type : 'self.oneAgenda_xxx'.

        Les informations à récupérer se trouvent :
         1. dans les préférences de l'application (self.preferences.ade_settings)
         2. dans 'self.actionGroup_affichage'
         3. dans 'self.radioButton_FiltrePeriode-xxx' qui définissent la période affichée.

        En fonction de la nature du 'sender' on effectue ou non les mises à jour des variables.
        Si le 'sender' est 'None', ce n'est pas un signal qui a demandé l'affichage : a priori c'est donc le 1er
        affichage demandé au lancement de l'application : il faut alors initialiser toutes les variables.
        """

        # Récupération du style d'affichage de l'agenda
        if sender in [self.actionGroup_affichage, None]:
            if self.action_affichageGraphique.isChecked():
                self.oneAgenda_style = AgendaStyle.GRAPHIC
            elif self.action_affichageListe.isChecked():
                self.oneAgenda_style = AgendaStyle.TABLE
            else:
                self.oneAgenda_style = AgendaStyle.STATISTICS

        # Si le sender est un des boutons de choix de mode d'affichage,
        # aucun autre paramètre n'a changé. On sort.
        if sender is self.actionGroup_affichage:
            return

        # Récupération des heures de début et de fin de journée affichée (propre à AgendaStyle.GRAPHIC)
        if sender in [self.actionPreferences, self.oneAgenda, None]:
            self.oneAgenda_begin_day_time = self.preferences.settings["affichage/journeeHoraires"].value.begin_arrow
            self.oneAgenda_end_day_time = self.preferences.settings["affichage/journeeHoraires"].value.end_arrow

        # Récupération des dates de début et de fin du calendrier affichées.
        # Ces dates sont aussi utilisées comme bornes pour la recherche de créneaux libres.
        if sender in [self.actionPreferences, self.oneShotTimer, None]:
            if self.radioButton_FiltrePeriodeAnnee.isChecked():
                self.oneAgenda_begin_date = self.preferences.settings["annee/annee"].value.begin_arrow
                self.oneAgenda_end_date = self.preferences.settings["annee/annee"].value.end_arrow
            elif self.radioButton_FiltrePeriodeFin.isChecked():
                today = self.oneAgenda_begin_date = arrow.Arrow.utcnow()
                S1_fin = self.preferences.settings['annee/semestre1'].value.end_arrow
                S2_fin = self.preferences.settings['annee/semestre2'].value.end_arrow
                annee_fin = self.preferences.settings['annee/annee'].value.end_arrow
                if today <= S1_fin:
                    self.oneAgenda_end_date = S1_fin
                elif today <= S2_fin:
                    self.oneAgenda_end_date = S2_fin
                elif today <= annee_fin:
                    self.oneAgenda_end_date = annee_fin
                else:
                    self.oneAgenda_begin_date = self.oneAgenda_end_date = annee_fin
            elif self.radioButton_FiltrePeriodeS1.isChecked():
                self.oneAgenda_begin_date = self.preferences.settings["annee/semestre1"].value.begin_arrow
                self.oneAgenda_end_date = self.preferences.settings["annee/semestre1"].value.end_arrow
            elif self.radioButton_FiltrePeriodeS2.isChecked():
                self.oneAgenda_begin_date = self.preferences.settings["annee/semestre2"].value.begin_arrow
                self.oneAgenda_end_date = self.preferences.settings["annee/semestre2"].value.end_arrow
            elif self.radioButton_FiltrePeriodeAutre.isChecked():
                self.oneAgenda_begin_date = arrow.Arrow(*self.dateEdit_FiltrePeriodeDebut.date().getDate())
                self.oneAgenda_end_date = arrow.Arrow(*self.dateEdit_FiltrePeriodeFin.date().getDate())
            else:
                self.oneAgenda_begin_date = self.preferences.settings["annee/annee"].value.begin_arrow
                self.oneAgenda_end_date = self.preferences.settings["annee/annee"].value.end_arrow
            # /!\ Les dates récupérées depuis 'self.preferences' sont 'timezone-naive'. Réglons le problème !
            self.oneAgenda_begin_date = self.oneAgenda_begin_date.replace(tzinfo=ADEcalendar.TimeZone)
            self.oneAgenda_end_date = self.oneAgenda_end_date.replace(tzinfo=ADEcalendar.TimeZone)
            # On établit la liste des dates interdites
            self.oneAgenda_holidays: list[arrow.Arrow] = self.holidays(self.oneAgenda_begin_date,
                                                                       self.oneAgenda_end_date)

        if sender in [self.actionPreferences, self.actionGroup_affichage, self.oneAgenda, None]:
            # Récupération de l'option 'cacher les semaines vides'
            self.oneAgenda_show_empty_weeks: bool = self.preferences.settings["affichage/semainesVides"].value
            # Récupération des jours de la semaine affichés (propre à AgendaStyle.GRAPHIC)
            self.oneAgenda_displayed_week_days = [i for i in range(7) if
                                                  self.preferences.settings[f"affichage/{i + 1}"].value]
            # Récupération des colonnes à afficher en mode "Liste"
            self.oneAgenda_displayed_columns = [key.replace("affichageListe/", "")
                                                for key in self.preferences.settings.keys()
                                                if key.startswith("affichageListe/") and
                                                self.preferences.settings[key].value]

    def computeListOfCalendarsToDisplay(self):
        """
        Objectif : remplir la liste 'self.oneAgenda_cal_to_display' avec la liste des agendas à afficher.

        Trois modes sont possibles :
         1. Mode Simple : on remplit avec les agendas sélectionnés
         2. Mode Freetime : on construit 1 agenda rempli de créneaux libres pour les agendas sélectionnés.
         3. Mode Together : on construit 1 agenda rempli d'événements communs aux agendas sélectionnés.
        """

        if not self.resourceTreeStructure:
            return

        # On parcourt les items de la liste 'self.ressources_item_list'.
        # On récupère l'agenda des items si
        #  - l'item est une ressource,
        #  - l'item a un agenda (s'il n'a jamais été sélectionné, l'item n'a pas encore d'agenda associé),
        #  - l'agenda est marqué comme 'isSelected())

        selected_cals: list[ADEcalendar] = list()
        for node in PreOrderIter(self.resourceTreeStructure.root_item):
            assert isinstance(node, ResourceNode)
            if node.isResource() and node.isSelected:
                if node.merged_calendar:
                    if self.checkBox_display_extra_calendars.isChecked():
                        selected_cals.append(node.merged_calendar)
                    else:
                        selected_cals.append(node.calendar)
                    continue
                if node.calendar:
                    selected_cals.append(node.calendar)
                if node.extra_calendars:
                    selected_cals.extend(node.extra_calendars)

        # Option "Affichage simple" sélectionnée
        if self.action_modeSimple.isChecked():
            self.oneAgenda_cal_to_display = selected_cals
            return

        # Option "Créneaux libres" sélectionnée
        if self.action_modeFreetime.isChecked():
            # On établit le dictionnaire des horaires journaliers de cours
            # La clé est le numéro du jour de la semaine (1 pour le lundi, ...)
            free_dict_horaires_journee = defaultdict(lambda: list())
            for isoWeekday in range(1, 8):
                _dayKey = f"horaires/{isoWeekday}"
                # Si la clé principale du jour n'existe pas ou est 'False', on passe au jour suivant
                if (_dayKey not in self.preferences.settings.keys()) or (not self.preferences.settings[_dayKey].value):
                    continue
                if f"{_dayKey}/AM" in self.preferences.settings.keys() and \
                        self.preferences.settings[_dayKey + "/AM"].value and \
                        f"{_dayKey}/AMtime" in self.preferences.settings.keys():
                    free_dict_horaires_journee[isoWeekday].append(
                        [self.preferences.settings[f"{_dayKey}/AMtime"].value.begin_arrow,
                         self.preferences.settings[f"{_dayKey}/AMtime"].value.end_arrow])
                if f"{_dayKey}/PM" in self.preferences.settings.keys() and \
                        self.preferences.settings[_dayKey + "/PM"].value and \
                        f"{_dayKey}/PMtime" in self.preferences.settings.keys():
                    free_dict_horaires_journee[isoWeekday].append(
                        [self.preferences.settings[f"{_dayKey}/PMtime"].value.begin_arrow,
                         self.preferences.settings[f"{_dayKey}/PMtime"].value.end_arrow])

            # On crée un ADECalendar() rempli par les créneaux libres fusionnés
            freetime_cal = ADEcalendar.freeTime(cal_list=selected_cals,
                                                date_debut=self.oneAgenda_begin_date,
                                                date_fin=self.oneAgenda_end_date,
                                                plages_horaires_autorisees=free_dict_horaires_journee,
                                                dates_interdites=self.oneAgenda_holidays)
            # On vide la liste des agendas à afficher et on y place notre nouvel agenda rempli
            # par les créneaux libres
            self.oneAgenda_cal_to_display.clear()
            self.oneAgenda_cal_to_display.append(freetime_cal)
            return

        # Option "Points communs" sélectionnée
        elif self.action_modeTogether.isChecked():
            # ... on crée le calendrier correspondant aux point communs ...
            intersection_cal = ADEcalendar.commonEvents(*selected_cals)
            # ... et seul ce calendrier sera affiché.
            self.oneAgenda_cal_to_display.clear()
            self.oneAgenda_cal_to_display.append(intersection_cal)
            return

    @pyqtSlot()
    def updateAgendaView(self):
        """
        Objectif : mettre à jour le visuel des agendas à afficher.
        """
        sender = self.sender()
        snapshot: bool = False

        # On fait patienter l'utilisateur
        QApplication.setOverrideCursor(Qt.CursorShape.WaitCursor)

        # Est-ce un snapshot ?
        if sender == self.action_agendaSnapshot:
            snapshot = True

        # Est-ce que les préférences viennent d'être mises à jour ?
        preferences_changed = sender is self.actionPreferences

        # Récupération et ajustement des paramètres
        self.computeAgendaParameters(sender)

        # Mise à jour du filtre d'événements
        self.updateFilter(sender)

        # Construction de la liste des agendas à afficher
        self.computeListOfCalendarsToDisplay()

        # On initie un nouvel agenda du type demandé ou on récupère celui déjà existant s'il existe
        if self.oneAgenda_style == AgendaStyle.GRAPHIC:
            _oneAgenda = OneAgendaGraphicView(cal_list=self.oneAgenda_cal_to_display,
                                              begin_date=self.oneAgenda_begin_date,
                                              end_date=self.oneAgenda_end_date,
                                              begin_day_time=self.oneAgenda_begin_day_time,
                                              end_day_time=self.oneAgenda_end_day_time,
                                              show_empty_weeks=self.oneAgenda_show_empty_weeks,
                                              displayed_week_days=self.oneAgenda_displayed_week_days,
                                              holidays=self.oneAgenda_holidays,
                                              event_filter=self.oneAgenda_event_filter,
                                              parent=None)
            if not snapshot:
                # _oneAgenda.signalMoveEvent.connect(lambda details: self.moveEventRequired(details))
                _oneAgenda.signalMessage.connect(lambda message: self.statusBarSecondLabel.setText(message))
                _oneAgenda.signalOpenPreferences.connect(lambda: self.openPreferences("Affichage graphique"))
        elif self.oneAgenda_style == AgendaStyle.TABLE:
            _oneAgenda = OneAgendaTableView(cal_list=self.oneAgenda_cal_to_display,
                                            begin_date=self.oneAgenda_begin_date,
                                            end_date=self.oneAgenda_end_date,
                                            displayed_columns=self.oneAgenda_displayed_columns,
                                            preferences_changed=preferences_changed,
                                            event_filter=self.oneAgenda_event_filter,
                                            parent=None)
            if not snapshot:
                _oneAgenda.signalMessage.connect(lambda message: self.statusBarSecondLabel.setText(message))
        elif self.oneAgenda_style == AgendaStyle.STATISTICS:
            _oneAgenda = OneAgendaStatistics(cal_list=self.oneAgenda_cal_to_display,
                                             begin_date=self.oneAgenda_begin_date,
                                             end_date=self.oneAgenda_end_date,
                                             event_filter=self.oneAgenda_event_filter,
                                             parent=None)
            if not snapshot:
                _oneAgenda.signalMessage.connect(lambda message: self.statusBarSecondLabel.setText(message))
            QApplication.restoreOverrideCursor()
        else:
            QApplication.restoreOverrideCursor()
            return

        # On connecte les signaux du nouvel agenda pour suivre la progression de sa construction
        _oneAgenda.signalProgressMessage.connect(lambda progress, msg: self.statusProgressBar.setValue(progress))
        _oneAgenda.signalProgressMessage.connect(lambda progress, msg: self.statusProgressBar.setFormat(msg))

        # On construit l'agenda.
        # Cette étape ***PREND DU TEMPS***. Mesurons le temps écoulé.
        # La progression est suivi grâce aux signaux de l'agenda.
        _time = timeit.timeit(lambda: _oneAgenda.build(), number=1)
        self.statusProgressBar.setFormat(f"Rendu terminé en {int(1e3 * _time)} ms")

        # Si un snapshot est demandé, on l'ajoute à la liste, on lui donne un titre de fenêtre et on l'affiche
        if snapshot:
            self.agendaSnapshots.append(_oneAgenda)
            window_title = f"Agenda dupliqué - " \
                           f"{str(self.oneAgenda_style)} - " \
                           f"Ressources : {', '.join([cal.altTitle for cal in self.oneAgenda_cal_to_display])} - " \
                           f"du {self.oneAgenda_begin_date.format('DD/MM/YYYY', 'fr_fr')} " \
                           f"au {self.oneAgenda_end_date.format('DD/MM/YYYY', 'fr_fr')}"
            self.agendaSnapshots[-1].setWindowTitle(window_title)
            self.agendaSnapshots[-1].show()

        # Si ce n'est pas un snapshot, on retire l'agenda précédent et on le marque pour destruction
        # puis on place le nouveau dans le réceptacle ('self.AgendaHolder.layout()').
        if not snapshot:
            try:
                self.AgendaHolder.layout().removeWidget(self.oneAgenda)
                self.oneAgenda.deleteLater()
                del self.oneAgenda
            except (AttributeError, RuntimeError):
                pass
            self.oneAgenda = _oneAgenda
            self.AgendaHolder.layout().addWidget(self.oneAgenda)

        QApplication.restoreOverrideCursor()
        self.statusBarMainLabel.setText((f"{self.oneAgenda.countEvent} événements répertoriés, d'une durée totale de "
                                         f"{formatDuration_hhmm(self.oneAgenda.totalDuration)}."))

    # def moveEventRequired(self, details: EventDetails):
    #     message = QMessageBox(self)
    #     message.setTextFormat(Qt.RichText)
    #     message.setStandardButtons(QMessageBox.Ok | QMessageBox.StandardButton.Cancel)
    #     message.setWindowTitle("Déplacer cette activité ?")
    #     message.setText(f"{details.tool_tip()}")
    #     if not message.exec() == QMessageBox.Ok:
    #         return
    #
    #     self.clearFilter(do_not_clear_period=True)
    #     self.action_modeFreetime.toggle()
    #     self.comboBox_FiltreDureeLogical.setCurrentIndex(0)
    #     self.timeEdit_FiltreDuree.setTime(QTime(details.dureeH, details.dureeM))
    #     self.updateAgendaView()
    #
    #     html = f"<p>L'affichage vient de basculer en mode 'Créneaux libres'.<br>" \
    #            f"Seuls apparaissent les créneaux dont la durée est supérieure ou égale à la " \
    #            f"durée de la séance à décaler ({details.dureeJJHHMM}).</p>" \
    #            f"Après avoir lu ces instructions :<ol>" \
    #            f"<li>fermer cette fenêtre en cliquant sur OK,</li>" \
    #            f"<li>sélectionner <b>l'ensemble " \
    #            f"des ressources</b> participant à cette activité :" \
    #            f"{details.participantsRichText}</li>" \
    #            f"<li>rechercher et cliquer sur le créneau libre.</li></ol>"
    #     self.dockWidget_Assistant.setVisible(True)
    #     self.textEdit_Assistant.textCursor().insertHtml(html)
    #
    # def moveEventInProgress(self, sender: QPushButton):
    #     if sender.text() != '&OK':
    #         return
    #     self.eventMovePending: bool = True
    #     self.dockWidget_Assistant.show()

    def new_year_wizard(self, auto_launch: bool = False):
        if auto_launch:
            if not self.preferences.settings['wizards/newYearOnOff'].value:
                return
            t = datetime.date.today()
            e = self.preferences.settings["annee/annee"].value.end_arrow.date()
            if (days := (e - t).days) > self.preferences.settings['wizards/newYearDays'].value:
                return
            if days > 0:
                s = f"se termine dans {days} jours"
            else:
                s = f"est terminée"

            msg = QMessageBox()
            msg.setIcon(QMessageBox.Icon.Question)
            msg.setWindowTitle("Assistant de migration")
            msg.setText(f"<p>L'année universitaire {s}. "
                        "ADEexplorer vous propose un assistant pour faciliter la migration.</p>"
                        "<p>Voulez-vous lancer l'assistant maintenant ?</p>"
                        "<p>Si vous répondez Non, vous pourrez lancer cet assistant plus "
                        "tard via le menu <tt>Assistants</tt>.</p>")
            msg.setStandardButtons(QMessageBox.StandardButton.No | QMessageBox.StandardButton.Yes)
            cb = QCheckBox("Ne plus proposer l'assistant au démarrage")
            cb.setChecked(not self.preferences.settings['wizards/newYearOnOff'].value)
            msg.setCheckBox(cb)
            rep = msg.exec()
            self.preferences.settings.widget('wizards/newYearOnOff').setChecked(not cb.isChecked())
            self.preferences.settings['wizards/newYearOnOff'].value = not cb.isChecked()
            if rep == QMessageBox.StandardButton.No:
                return

        transistion = NewYearWizard(self.preferences, self.resourceTreeStructure)
        if transistion.exec() == QDialog.DialogCode.Rejected:
            return
        self.preferences.settings["annee/annee"].value = DateRange(transistion.year_begin.date(),
                                                                   transistion.year_end.date())
        self.preferences.settings["annee/semestre1"].value = DateRange(transistion.s1_begin.date(),
                                                                       transistion.s1_end.date())
        self.preferences.settings["annee/semestre2"].value = DateRange(transistion.s2_begin.date(),
                                                                       transistion.s2_end.date())
        self.preferences.settings["vacances/toussaint"].value = DateRange(transistion.toussaint_begin.date(),
                                                                          transistion.toussaint_end.date())
        self.preferences.settings["vacances/noel"].value = DateRange(transistion.noel_begin.date(),
                                                                     transistion.noel_end.date())
        self.preferences.settings["vacances/hiver"].value = DateRange(transistion.hiver_begin.date(),
                                                                      transistion.hiver_end.date())
        self.preferences.settings["vacances/paques"].value = DateRange(transistion.paques_begin.date(),
                                                                       transistion.paques_end.date())
        self.preferences.settings["gestion/projectID"].value = transistion.project_id

        self.clearCache()
        self.clearFilter()

    @staticmethod
    def help():
        QDesktopServices.openUrl(QUrl(__gitlab__ + "-/blob/master/Readme.md"))

    @staticmethod
    def contactAuthor():
        QDesktopServices.openUrl(QUrl("mailto:vincent.raspal@uca.fr?"
                                      f"subject=[ADEexplorer_v{__ade_explorer_version__} "
                                      f"({sys.platform.capitalize()})] "
                                      f"mon sujet ici"))

    def showCollisions(self, node: ResourceNode, force_window: bool = False):
        if not node.extra_url:
            return

        if not (node.calendar and node.extra_calendars):
            self.loadCalendar(node)

        if not (node.calendar and node.extra_calendars):
            return

        collisions: list[Collision] = Collision.intercalendar_collision_list(node.calendar, *node.extra_calendars,
                                                                             after=arrow.Arrow.utcnow())
        if not collisions:
            return

        _hidden_collisions: list[Collision] = list()
        try:
            with open(pM.collisions_file, 'r') as f:
                file_content = f.read()
                if file_content:
                    for entry in json.loads(file_content):
                        _hidden_collisions.append(Collision(*Calendar(imports=entry).events))
        except FileNotFoundError:
            pass

        for collision in collisions:
            collision.event1.colliding_events.add(collision.event2)
            collision.event2.colliding_events.add(collision.event1)
            if collision in _hidden_collisions:
                collision.hidden = True

        if all(c.hidden for c in collisions) and not force_window:
            return

        dial = CollisionsDialog(collisions, cal_name=node.name)
        r = dial.exec()
        if r == QDialog.DialogCode.Rejected:
            return

        with open(pM.collisions_file, 'w') as f:
            f.write(json.dumps([str(str(Calendar(events={c.event1, c.event2})))
                                for c in collisions if c.hidden], indent=2))
