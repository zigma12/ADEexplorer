#!/usr/bin/python3

import sys
import pathsManagement as pM

from PyQt5.QtCore import QTranslator
from PyQt5.QtWidgets import QApplication

from mainWindow import MainWindow, __ade_explorer_version__


app = QApplication(sys.argv)

# Traduction des boutons standards
qtTranslator = QTranslator()
if qtTranslator.load(pM.tr_file, pM.tr_dir):
    app.installTranslator(qtTranslator)


if sys.platform in ("win32", 'darwin'):
    # Le style natif de base de MacOS pose des problèmes d'affichage
    app.setStyle('Fusion')


app.setStyleSheet("QToolTip {color: #ffffff;"
                  "background-color: #202020;"
                  "border: 0px;"
                  "opacity: 230;"
                  "padding: 5px;"
                  "border-radius:4px;"
                  "}")

print(f"Version:              {__ade_explorer_version__}")
print(f"Platform:             {sys.platform.capitalize()}")
print(f"Executable directory: {pM.exe_dir}")
print(f"Executable file:      {pM.exe_file}")
print(f"Bundle directory:     {pM.bundle_dir}")
print(f"Ui's directory:       {pM.ui_dir}")
print(f"Cache directory:      {pM.cache_dir}")
print(f"Settings file:        {pM.settings_file}")
print('-'*73)


window = MainWindow()
window.show()
app.exec()
