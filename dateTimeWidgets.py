import arrow
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QDate, QTime, pyqtSignal
from PyQt5.QtWidgets import QApplication


class DateRange:
    separator = '-->'
    dateFormat = "dd/MM/yyyy"

    def __init__(self, begin: QDate = QDate(2020, 1, 1), end: QDate = QDate(2020, 1, 1)):
        self._begin = begin
        self._end = end

    @property
    def begin(self):
        return self._begin

    @begin.setter
    def begin(self, val: QDate):
        self._begin = val

    @property
    def begin_arrow(self):
        return arrow.Arrow(self._begin.year(), self._begin.month(), self._begin.day())

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, val: QDate):
        self._end = val

    @property
    def end_arrow(self):
        return arrow.Arrow(self._end.year(), self._end.month(), self._end.day())

    def unserialize(self, data: str):
        self.begin, self.end = (QDate.fromString(frag, DateRange.dateFormat)
                                for frag in data.split(DateRange.separator))

    @property
    def serialize(self):
        return (self.begin.toString(DateRange.dateFormat) +
                DateRange.separator +
                self.end.toString(DateRange.dateFormat))

    def __repr__(self):
        return self.serialize

    def __str__(self):
        return self.serialize


class TimeRange:
    separator = '-->'
    timeFormat = "HH:mm"

    def __init__(self, begin: QTime = QTime(0, 0, 0), end: QTime = QTime(0, 0, 0)):
        self._begin = begin
        self._end = end

    @property
    def begin(self):
        return self._begin

    @begin.setter
    def begin(self, val: QTime):
        self._begin = val

    @property
    def begin_arrow(self):
        return arrow.Arrow(2000, 1, 1, self._begin.hour(), self._begin.minute(), self._begin.second())

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, val: QTime):
        self._end = val

    @property
    def end_arrow(self):
        return arrow.Arrow(2000, 1, 1, self._end.hour(), self._end.minute(), self._end.second())

    def unserialize(self, data: str):
        self.begin, self.end = (QTime.fromString(frag, TimeRange.timeFormat)
                                for frag in data.split(DateRange.separator))

    @property
    def serialize(self):
        return (self.begin.toString(TimeRange.timeFormat) +
                TimeRange.separator +
                self.end.toString(TimeRange.timeFormat))

    def __repr__(self):
        return self.serialize

    def __str__(self):
        return self.serialize


class DateRangeWidget(QtWidgets.QWidget):
    dateFormat = "ddd dd MMM yyyy"
    editingFinished = pyqtSignal()

    def __init__(self):
        super(DateRangeWidget, self).__init__()

        self.setObjectName("dateRangeWidget")

        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.groupBox = QtWidgets.QGroupBox(self)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout.setContentsMargins(5, 5, 5, 2)
        self.gridLayout = QtWidgets.QGridLayout()
        self.beginLabel = QtWidgets.QLabel(self.groupBox)
        self.beginLabel.setText("Début :")
        self.gridLayout.addWidget(self.beginLabel, 0, 0)
        self.beginDateEdit = QtWidgets.QDateEdit(self.groupBox)
        self.beginDateEdit.setDisplayFormat(DateRangeWidget.dateFormat)
        self.beginDateEdit.setCalendarPopup(True)
        self.beginDateEdit.setDate(QtCore.QDate(2020, 1, 1))
        self.beginDateEdit.setMinimumWidth(160)
        self.gridLayout.addWidget(self.beginDateEdit, 0, 1)
        self.endLabel = QtWidgets.QLabel(self.groupBox)
        self.gridLayout.addWidget(self.endLabel, 1, 0)
        self.endDateEdit = QtWidgets.QDateEdit(self.groupBox)
        self.endDateEdit.setDisplayFormat(DateRangeWidget.dateFormat)
        self.endDateEdit.setCalendarPopup(True)
        self.endDateEdit.setDate(QtCore.QDate(2020, 1, 1))
        self.beginDateEdit.setMinimumWidth(160)
        self.endLabel.setText("Fin :")
        self.gridLayout.addWidget(self.endDateEdit, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout)
        self.horizontalLayout_2.addWidget(self.groupBox)

        self.beginDateEdit.editingFinished.connect(
            lambda: self.endDateEdit.setMinimumDate(self.beginDateEdit.date()))

        self.beginDateEdit.editingFinished.connect(self.editingFinished.emit)
        self.endDateEdit.editingFinished.connect(self.editingFinished.emit)

        QtCore.QMetaObject.connectSlotsByName(self)

    def within(self, other):
        assert isinstance(other, DateRangeWidget)
        other.beginDateEdit.editingFinished.connect(
            lambda: self.beginDateEdit.setMinimumDate(other.beginDateEdit.date())
        )
        other.endDateEdit.editingFinished.connect(
            lambda: self.endDateEdit.setMaximumDate(other.endDateEdit.date())
        )
        other.beginDateEdit.editingFinished.connect(
            lambda: self.endDateEdit.setMinimumDate(other.beginDateEdit.date())
        )
        other.endDateEdit.editingFinished.connect(
            lambda: self.beginDateEdit.setMaximumDate(other.endDateEdit.date())
        )

    def after(self, other):
        assert isinstance(other, DateRangeWidget)
        other.endDateEdit.editingFinished.connect(
            lambda: self.beginDateEdit.setMinimumDate(other.endDateEdit.date().addDays(1))
        )

    def before(self, other):
        other.after(self)

    def setBoxTitle(self, title: str):
        self.groupBox.setTitle(title)


class TimeRangeWidget(QtWidgets.QWidget):
    timeFormat = "HH:mm"
    timeResolution_in_min = 15
    editingFinished = pyqtSignal()

    def __init__(self):
        super(TimeRangeWidget, self).__init__()
        self._timeRange: TimeRange = None

        self.setObjectName("timeRangeWidget")
        self.resize(230, 86)

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Expanding,
                                           QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)

        self.horizontalLayout = QtWidgets.QHBoxLayout(self)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.beginLabel = QtWidgets.QLabel(self)
        self.beginLabel.setObjectName("debutLabel")
        self.beginLabel.setText("de :")
        self.horizontalLayout.addWidget(self.beginLabel)
        self.beginTimeEdit = QtWidgets.QTimeEdit(self)
        self.beginTimeEdit.setDisplayFormat(TimeRangeWidget.timeFormat)
        self.beginTimeEdit.setCalendarPopup(True)
        self.beginTimeEdit.setTime(QtCore.QTime(0, 0))
        self.beginTimeEdit.setObjectName("debut")
        self.beginTimeEdit.setSizePolicy(sizePolicy)
        self.horizontalLayout.addWidget(self.beginTimeEdit)
        self.endLabel = QtWidgets.QLabel(self)
        self.endLabel.setObjectName("finLabel")
        self.horizontalLayout.addWidget(self.endLabel)
        self.endTimeEdit = QtWidgets.QTimeEdit(self)
        self.endTimeEdit.setDisplayFormat(TimeRangeWidget.timeFormat)
        self.endTimeEdit.setCalendarPopup(True)
        self.endTimeEdit.setTime(QtCore.QTime(0, 0))
        self.endTimeEdit.setObjectName("fin")
        self.endLabel.setText("à :")
        self.endTimeEdit.setSizePolicy(sizePolicy)
        self.horizontalLayout.addWidget(self.endTimeEdit)

        self.beginTimeEdit.editingFinished.connect(self.roundTimeToResolution)
        self.endTimeEdit.editingFinished.connect(self.roundTimeToResolution)

        self.beginTimeEdit.editingFinished.connect(lambda: self.endTimeEdit.setMinimumTime(self.beginTimeEdit.time()))
        self.beginTimeEdit.editingFinished.connect(self.editingFinished.emit)
        self.endTimeEdit.editingFinished.connect(self.editingFinished.emit)

        QtCore.QMetaObject.connectSlotsByName(self)

    def roundTimeToResolution(self):
        tE = self.sender()
        time_in_min = round(tE.time().msecsSinceStartOfDay() / 1000 / 60)
        rounded_time_in_min = self.timeResolution_in_min * round(time_in_min / self.timeResolution_in_min)
        new_time = QtCore.QTime().fromMSecsSinceStartOfDay(rounded_time_in_min * 60 * 1000)
        tE.setTime(new_time)

    def within(self, other):
        assert isinstance(other, TimeRangeWidget)
        other.beginTimeEdit.editingFinished.connect(
            lambda: self.beginTimeEdit.setMinimumTime(other.beginTimeEdit.time())
        )
        other.endTimeEdit.editingFinished.connect(
            lambda: self.endTimeEdit.setMaximumTime(other.endTimeEdit.time())
        )
        other.beginTimeEdit.editingFinished.connect(
            lambda: self.endTimeEdit.setMinimumTime(other.beginTimeEdit.time())
        )
        other.endTimeEdit.editingFinished.connect(
            lambda: self.beginTimeEdit.setMaximumTime(other.endTimeEdit.time())
        )

    def after(self, other):
        assert isinstance(other, TimeRangeWidget)
        other.endTimeEdit.editingFinished.connect(
            lambda: self.beginTimeEdit.setMinimumTime(other.endTimeEdit.time())
        )

    def before(self, other):
        other.after(self)


if __name__ == '__main__':
    app = QApplication([])
    settings = DateRangeWidget()
    settings.show()
    app.exec()
