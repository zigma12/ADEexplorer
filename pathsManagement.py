import inspect
import os
import sys
from os import path

# noinspection PyUnresolvedReferences
from PyQt5 import uic
from PyQt5.QtCore import QLocale

if hasattr(sys, 'frozen'):
    """Executable pyInstaller"""
    bundle_dir = sys._MEIPASS
    exe_file = sys.executable
    exe_dir = path.dirname(exe_file)

else:
    """Mode normal"""
    # Le fichier exécuté est le dernier de la liste dans 'inspect.stack()'
    exe_file = inspect.stack()[-1].filename
    bundle_dir = path.dirname(exe_file)
    exe_dir = path.dirname(exe_file)

cache_dir = os.path.join(exe_dir, ".cache", "")
# **********************************
# v1.4.0 : on est passé de ".calendar_cache" à ".cache".
# Si l'ancien dossier existe mais pas le nouveau, on le renomme
# Ligne à supprimer plus tard
if (not os.path.isdir(cache_dir) and
        os.path.isdir(os.path.join(exe_dir, ".calendar_cache", ""))):
    os.rename(os.path.join(exe_dir, ".calendar_cache", ""),
              cache_dir)
# ***********************************
ui_dir = os.path.join(bundle_dir, "ui", "")
tr_dir = os.path.join(bundle_dir, 'tr', "")
tr_file = "qt_" + QLocale.system().name()
rsc_dir = os.path.join(bundle_dir, 'resources', "")
ico_dir = os.path.join(rsc_dir, "icons", "")
pix_dir = os.path.join(rsc_dir, "pix", "")

comments_file = os.path.join(cache_dir, "comments.json")
collisions_file = os.path.join(cache_dir, "collisions.json")
treeview_expansion_file = os.path.join(cache_dir, "treeviews.json")
settings_file = os.path.join(exe_dir, "ADEexplorer.ini")

# Création des fichiers s'ils n'existent pas
for file in (comments_file, collisions_file, treeview_expansion_file, settings_file):
    if not path.isfile(file):
        os.makedirs(path.dirname(file), exist_ok=True)
        open(file, 'a').close()


def loadUi(widget, f):
    uic.loadUi(os.path.join(ui_dir, f), widget)


def rsc(name: str):
    return os.path.join(rsc_dir, name)


def icon(name: str):
    return os.path.join(ico_dir, name)


def pix(name: str):
    return os.path.join(pix_dir, name)
