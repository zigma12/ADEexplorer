This project has moved to [https://gitlab.com/vinraspa/ADEexplorer/](https://gitlab.com/vinraspa/ADEexplorer/)

This repository is frozen and kept for backward compatibity only (especially file `binaries` used for updates research).

**Do not commit here.**
