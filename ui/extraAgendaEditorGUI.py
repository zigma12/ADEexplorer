# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/vincent/STORAGE_V/Seafile/ADEexplorer/src/ui/extraAgendaEditorGUI.ui'
#
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ExtraAgendaEditor(object):
    def setupUi(self, ExtraAgendaEditor):
        ExtraAgendaEditor.setObjectName("ExtraAgendaEditor")
        ExtraAgendaEditor.resize(482, 198)
        self.formLayout = QtWidgets.QFormLayout(ExtraAgendaEditor)
        self.formLayout.setObjectName("formLayout")
        self.label_2 = QtWidgets.QLabel(ExtraAgendaEditor)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.lineEdit_url = QtWidgets.QLineEdit(ExtraAgendaEditor)
        self.lineEdit_url.setObjectName("lineEdit_url")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.lineEdit_url)
        self.label = QtWidgets.QLabel(ExtraAgendaEditor)
        self.label.setObjectName("label")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label)
        self.lineEdit_name = QtWidgets.QLineEdit(ExtraAgendaEditor)
        self.lineEdit_name.setObjectName("lineEdit_name")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.lineEdit_name)
        self.label_3 = QtWidgets.QLabel(ExtraAgendaEditor)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.lineEdit_user = QtWidgets.QLineEdit(ExtraAgendaEditor)
        self.lineEdit_user.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.lineEdit_user.setObjectName("lineEdit_user")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.lineEdit_user)
        self.label_4 = QtWidgets.QLabel(ExtraAgendaEditor)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.lineEdit_password = QtWidgets.QLineEdit(ExtraAgendaEditor)
        self.lineEdit_password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit_password.setObjectName("lineEdit_password")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.lineEdit_password)
        self.label_5 = QtWidgets.QLabel(ExtraAgendaEditor)
        self.label_5.setObjectName("label_5")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_5)
        self.pushButton_color = QtWidgets.QPushButton(ExtraAgendaEditor)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_color.sizePolicy().hasHeightForWidth())
        self.pushButton_color.setSizePolicy(sizePolicy)
        self.pushButton_color.setText("")
        self.pushButton_color.setObjectName("pushButton_color")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.pushButton_color)
        self.buttonBox = QtWidgets.QDialogButtonBox(ExtraAgendaEditor)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.FieldRole, self.buttonBox)

        self.retranslateUi(ExtraAgendaEditor)
        QtCore.QMetaObject.connectSlotsByName(ExtraAgendaEditor)

    def retranslateUi(self, ExtraAgendaEditor):
        _translate = QtCore.QCoreApplication.translate
        ExtraAgendaEditor.setWindowTitle(_translate("ExtraAgendaEditor", "Éditeur d\'agenda externe"))
        self.label_2.setText(_translate("ExtraAgendaEditor", "Url de l\'agenda"))
        self.label.setText(_translate("ExtraAgendaEditor", "Nom de l\'agenda :"))
        self.label_3.setText(_translate("ExtraAgendaEditor", "Identifiant :"))
        self.label_4.setText(_translate("ExtraAgendaEditor", "Mot de passe :"))
        self.label_5.setText(_translate("ExtraAgendaEditor", "Couleur de l\'agenda :"))
