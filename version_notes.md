# ADEexplorer v1.4.2 : notes de version

##  Nouveautés
 * __Assistant de migration vers une nouvelle année universitaire__ : 
   ADEexplorer peut à présent assister l'utilisateur dans le paramétrage 
   d'une nouvelle année. Cet outil permet de donner des valeurs par défaut 
   pour :
   * les dates de début et de fin d'année et de semestre ;
   * les dates de début et de fin des vacances universitaires ;
   * la valeur du `ProjectId`
   
    Quelques remarques :
   * Pour les vacances, ADEexplorer télécharge les données depuis
      [https://www.data.gouv.fr/fr/datasets/le-calendrier-scolaire/](https://www.data.gouv.fr/fr/datasets/le-calendrier-scolaire/).
      __Attention :__ la durée des vacances universitaires est généralement 
     différente de celle des vacances scolaires pour la Toussaint et les 
     vacances d'hiver. Il faut donc ajuster les dates proposées.

   * Pour le `ProjectId`, ADEexplorer teste différentes possibilités et 
     télécharge des agendas jusqu'à en trouver un qui contient des événement 
     planifiés (il ne faut donc pas lancer cette recherche trop tôt).

   ADEexplorer propose automatiquement le lancement de cet assistant 30 
   jours avant la fin de l'année en cours. Ce comportement est entièrement
   paramétrable dans les Préférences.

   
 * __Gestion des préférences__ : le système a été repensé de l'intérieur 
   afin d'être plus évolutif (invisible pour l'utilisateur) mais a également 
   fait peau neuve afin de s'adapter à un nombre croissant de catégories 
   (nouvelle entrée pour les `Assistants`)

##  Correction de bugs
ADEexplorer progresse grâce aux retours que font remonter les 
utilisateurs à l'auteur. Cette collaboration est précieuse. Pour faire 
remonter vos remarques : `Aide > Contacter l'auteur`. Merci.

 * En mode liste, la colonne `Nom` faisait apparaître le nom brut (incluant 
   le `module` et le `type`) depuis la mise à jour précédente. C'est corrigé.
 * Idem pour la fenêtre popup "Détails de l'événement"